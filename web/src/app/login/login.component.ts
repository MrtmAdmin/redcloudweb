import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from './authentication.service';
import { User } from '../usermanagement/user';
import { UserManagementService } from '../usermanagement/usermanagement.service';
import { LoginModel } from './login';
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';

@Component({
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    user = new User();
    loginObj = new LoginModel();
    error = '';
    submittedFlag: boolean = false;

    @ViewChild('userForm') form;

    constructor(
        private router: Router,
        private common: Common,
        private constant: Constant,
        private authService: AuthService,
        private userManagementService: UserManagementService,
        private activatedRoute: ActivatedRoute,
        private idle: Idle) { }

    ngOnInit() {

        //If link is login?action=logout then logging out
        this.activatedRoute.queryParams.subscribe(params => {
            let actionValue = params['action'];
            if (actionValue == "logout") {
                if (this.common.isLoggedIn()) {
                    this.authService.logout(false);
                }
            }
            else {
                if (this.common.isLoggedIn()) {
                    this.router.navigate([this.constant.dashboardUrl]);
                }
            }
        });
    }

    login() {
        this.submittedFlag = true;
        if (this.form.valid) {
            this.submittedFlag = false;
            let ctrl = this;
            // rcw:comment Shreya | Login API to check whether username and password is correct or not if true then return oauth2 token else return error.
            this.authService.login(this.loginObj.email, this.loginObj.password).then(function () {
                //Get logged User information after validating the login
                ctrl.userManagementService.getUserByUserName(ctrl.loginObj.email).then(function (userObj: User) {
                    localStorage.setItem(ctrl.constant.userObjName, ctrl.common.encrytStrng(JSON.stringify(userObj)));
                    ctrl.userManagementService.getUserAvatar(ctrl.loginObj.email)
                        .then(function (profilePicImage) {
                            if (profilePicImage != "" && profilePicImage !== null) {
                                localStorage.setItem(ctrl.constant.userProfilePic, ctrl.common.encrytStrng("data:image/jpeg;base64," + profilePicImage));
                            } else {
                                localStorage.setItem(ctrl.constant.userProfilePic, ctrl.common.encrytStrng(null));
                            }
                            document.cookie = "sessionTimeout=true";
                        }).then(function () {
                            // sets an idle timeout of 1800 seconds.
                            ctrl.idle.setIdle(ctrl.constant.idleTimeout);
                            // sets a timeout period of 1800 seconds. after another 1800
                            // seconds of inactivity which is  warning period,
                            // user would be logged out..
                            ctrl.idle.setTimeout(ctrl.constant.idleTimeout);
                            ctrl.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
                            // logout after timeout and  redirect to login
                            ctrl.idle.onTimeout.subscribe(() => {
                                ctrl.authService.logout(true);
                            });
                            //Starts the idle timer to watch when the user is idle
                            ctrl.idle.watch();
                            // return true to indicate successful login
                            ctrl.router.navigate(['/']);
                        })
                        .catch(function (error) {
                            console.log("Error:", error)
                        })
                }).catch(function (error) {
                    ctrl.error = JSON.parse(error._body).error_description;
                });
                // login successful if there's a oauth2 token in the response
            }).catch(function (error) {
                ctrl.error = JSON.parse(error._body).error_description;
            });
        }
    }
}
