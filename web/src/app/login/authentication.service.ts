import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Configuration } from '../services/configuration';
import { Idle } from '@ng-idle/core';
import { User } from '../usermanagement/user';
import { Constant } from '../common/constant';
import { Common } from '../common/common';
import { HttpService } from '../services/http.service';
import { Observable, Subscription } from 'rxjs/Rx';
import { Observer } from 'rxjs';

@Injectable()
export class AuthService {

    private actionUrl: string;
    configuration = new Configuration();
    constant = new Constant();
    common = new Common();
    token: string = '';
    private timer;
    isAuth: boolean = false;
    // Subscription object
    private sub = new Subscription();

    constructor(
        private http: HttpService,
        private _router: Router,
        private idle: Idle
    ) {
        this.actionUrl = this.configuration.ServerWithApiUrl;
    }

    // rcw:comment Shreya | Login API to backend to validate user and after successful validation
    // returns oauth2 json consisting of {access_token,refresh_token,expires_in}
    // so storing the access_token and refresh_token into local storage and generating timer for the expiration time using Observable
    login(email: string, password: string): Promise<any> {
        let ctrl = this;
        return this.http.put(this.actionUrl + "auth/user/login", { password: password, userName: email })
            .toPromise()
            .then(function (tokenResponse) {
                //oauth2 token json in response
                if (tokenResponse != null) {
                    let tokenJson = tokenResponse.json();
                    let tokenDetail = tokenJson['details'];
                    //Once the oauth2 token json is obtain extract refresh token and access token from the json and store in local storage
                    localStorage.setItem(ctrl.constant.tokenName, tokenDetail[ctrl.constant.tokenName]);
                    localStorage.setItem(ctrl.constant.refreshToken, tokenDetail[ctrl.constant.refreshToken]);
                    //Generate the timer to refresh the token according to the expiration time
                    if (tokenDetail['expires_in'] != null) {
                        ctrl.timer = Observable.timer((tokenDetail['expires_in'] * 1000) - 15000, (tokenDetail['expires_in'] * 1000) - 15000);
                        //Once the timer is called update the access_token in the local storage by sending the refresh_token
                        ctrl.sub = ctrl.timer.subscribe(function (t) {
                            ctrl.http.get(ctrl.actionUrl + "auth/refreshtoken?refresh_token=" + ctrl.common.getRefreshToken())
                                .toPromise()
                                .then(function (response) {
                                    //return this.http.post(this.actionUrl + "auth/refreshtoken", this.common.getRefreshToken())
                                    var returnedBody: any = (response.json())['details'];
                                    if (typeof returnedBody['access_token'] !== 'undefined') {
                                        localStorage.setItem(ctrl.constant.tokenName, returnedBody['access_token']);
                                        localStorage.setItem(ctrl.constant.refreshToken, returnedBody['refresh_token']);
                                    }
                                    else {
                                        // ctrl._router.navigate(['/login'], {queryParams: {action: "logout"}});
                                        ctrl.logout(true);
                                    }
                                })
                                .catch(function (error) {
                                    ctrl.logout(true);
                                    ctrl.common.handleError
                                });
                        });
                    }

                }
                return tokenResponse.json();
            })
            .catch(this.common.handleError);
    }

    // rcw:comment Shreya | Logout the user by clearing the local storage
    //  and un-subscribing the observable timer and stopping the idle timer
    logout(sessionTimeout: boolean): any {
        let ctrl = this;
        this.http.put(this.actionUrl + "auth/user/logout", {})
            .toPromise()
            .then(function () {
                // clear token remove user from local storage to log user out
                //ng2Idle parameters
                // and stop idle
                ctrl.idle.stop();
                localStorage.clear();
                ctrl.common.deleteAllCookies();
                ctrl.sub.unsubscribe();
                if (!sessionTimeout) {
                    ctrl._router.navigate([ctrl.constant.loginUrl]);
                } else {
                    ctrl.goToErrorPageDueToSessionTimeout();
                }
            }).catch(function () {
                // clear token remove user from local storage to log user out
                //ng2Idle parameters
                // and stop idle
                ctrl.idle.stop();
                localStorage.clear();
                ctrl.common.deleteAllCookies();
                ctrl.sub.unsubscribe();
                if (!sessionTimeout) {
                    ctrl._router.navigate([ctrl.constant.loginUrl]);
                } else {
                    ctrl.goToErrorPageDueToSessionTimeout();
                }

            });

    }

    // rcw:comment Shreya | Forgot password API to send email to the users
    forgotPassword(userName: string) {
        return this.http.put(this.actionUrl + "auth/user/mail/resetpassword", userName)
            .toPromise()
            .catch(this.common.handleError);
    }

    // rcw:comment Shreya | Reset password API to reset the password
    resetPassword(user: User) {
        return this.http.put(this.actionUrl + "auth/user/setpassword", user)
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);

    }

    // rcw:comment Shreya | Verify the token and email id - for forget password link and set password link
    verifyToken(userName: string, token: string): Promise<any> {
        return this.http.get(this.actionUrl + "auth/user/verifytoken?username=" + userName + "&" + "verification_token=" + token)
            .toPromise()
            .catch(this.common.handleError);
    }


    goToErrorPageDueToSessionTimeout() {
        this._router.navigate([this.constant.errorUrl], { queryParams: { action: "sessionTimeout" } });
    }

}
