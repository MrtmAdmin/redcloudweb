/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import 'rxjs/add/operator/toPromise';
import {Configuration} from '../services/configuration';
import {User} from '../usermanagement/user';
import {Role} from '../usermanagement/role';
import {Common} from '../common/common';

@Injectable()
export class UserManagementService {

    private actionUrl: string;
    constructor(private http: HttpService, private configuration: Configuration, private common: Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }

    getUsers(): Promise<User[]> {
        return this.http
            .get(this.actionUrl + 'usermanagement/users')
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }

    getById(id: number): Promise<User> {
        return this.http.get(this.actionUrl + "usermanagement/users/" + id)
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }

    getRolesWithUsers(): Promise<Role[]> {
        return this.http
            .get(this.actionUrl + 'usermanagement/roles/users')
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }

    add(user: User): Promise<any> {
        return this.http.post(this.actionUrl + "usermanagement/users", user)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    update(user: User): Promise<any> {
        return this.http.put(this.actionUrl + "usermanagement/users", user)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl + "usermanagement/users/" + id)
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }

    getUserByUserName(userName: string) {
        return this.http.get(this.actionUrl + "usermanagement/users/username/"+userName+"/")
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }

    checkNewUser(userName: string) {
        return this.http.get(this.actionUrl + "usermanagement/users/newuser/"+userName+"/")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    changePassword(email: string, password: string): Promise<any> {
        return this.http.put(this.actionUrl + "usermanagement/users/changepassword", {password: password, userName: email})
            .toPromise()
            .then()
            .catch(this.common.handleError);
    }

    uploadUserAvatar(file: any, userName: string): Promise<any> {
        let formData = new FormData();
        formData.append('file', file);
        userName = encodeURIComponent(userName);
        return this.http.put(this.actionUrl + "usermanagement/users/avatar/" + userName + "/", formData)
            .toPromise()
            .catch(this.common.handleError);
    }

    getUserAvatar(userName: string): Promise<any> {
        userName = encodeURIComponent(userName);
        return this.http.get(this.actionUrl + "usermanagement/users/avatar/" + userName + "/")
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }

    deleteUserAvatar(userName: string): Promise<any> {
        userName = encodeURIComponent(userName);
        return this.http.delete(this.actionUrl + "usermanagement/users/avatar/" + userName + "/")
            .toPromise()
            .catch(this.common.handleError);
    }

    getScreenAccessFromRoleId(id: number[]): Promise<any> {
        return this.http.get(this.actionUrl + "usermanagement/roles/screen/accesslevels/[" + id + "]")
            .toPromise()
            .then(response => (response.json())['details'])
            .catch(this.common.handleError);
    }
}

