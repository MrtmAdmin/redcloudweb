/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export class Segments {

    public id: number;
    public segmentName: string;
    public description:string;
    public status: string;
    public version: number;
    public criterias :any[];
    public objective: string;
    public isInProduction:boolean;
    public isInSimulation:boolean;
    public criteriaExpression:string;
    public updatable: boolean;
    public deletable: boolean;
    public archivable: boolean;
    public copyable: boolean;
    public versionable: boolean;
    public publishable: boolean;

    constructor() {
        this.criterias = [];
        this.isInProduction = false;
        this.isInSimulation = false;
        this.status = "DRAFT";
        this.updatable = true;
        this.deletable = true;
        this.archivable = true;
        this.copyable = false;
        this.versionable = false;
        this.publishable = true;
        this.criteriaExpression = "";
    };
}

export class SegmentCcrField {
    public id: number;
    public name: string;
    public operations: Object;
    public type:string;
    constructor() {};
}

export class SegmentCcrOperations {
    public id: number;
    public operator: string;
    public value: Object;
    public selectedVal : any[];
    public isEdit:boolean;
    constructor() {};
}
