/*
 * This is the suppression component which
 * binds the suppression HTML with the suppression service.
 * All the logic related to Suppression are mentioned in this component file.
 * @Author : Shreya
 */

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SuppressionService } from './suppression.service';
import { Suppression } from './suppression';
import { Generic } from '../models/generic';
import { ContextMenuService } from '../directives/angular2-contextmenu/src/contextMenu.service';
import { DataTable } from 'primeng/components/datatable/datatable';
import { CustomerContextRecordsService } from '../customercontextrecords/customer-context-records.service';
import * as _ from 'lodash';
import {ToasterService} from '../services/toaster.service';

@Component({
    moduleId: module.id,
    // rcw:todo | ramin | please consider to change the name to kebab-case naming suggestion:suppressions
    selector: 'suppressions',
    templateUrl: 'suppression.component.html',
})
export class SuppressionComponent implements OnInit, AfterViewInit {

    @ViewChild('configTable')
    configTable: DataTable;

    @ViewChild('simulTable')
    simulTable: DataTable;

    @ViewChild('prodTable')
    prodTable: DataTable;

    @ViewChild('suppressionForm') form;

    productionSuppressionList: Suppression[] = [];
    simulationSuppressionList: Suppression[] = [];
    configurationSuppressionList: Suppression[] = [];
    suppressionObj = new Suppression();
    showUpdateScreen = false;
    showEnlargedScreen = false;
    eventDetectorFlag = false;
    openAddPageFlag = false;
    showLargeListFlag = true;
    submittedFlag = false;
    dataSet: Generic[] = [];
    criteriaExp = '';
    filter = { 'group': { 'operator': 'AND', 'rules': [] } };
    configMenuOptions = [];
    modalContent = new Suppression();
    selectedTab = 'produc';
    selectedIndex: any = -1;
    isRequesting: boolean;
    behavioralDnas = [];
    archiveSuppressionDisplay = false;
    deleteSuppressionDisplay = false;
    cancelSuppressionDisplay = false;
    cancelChangesFlag: string = '';
    ruleBuilderSuppressionDisplay: boolean = false;
    popupIndex: number = -1;
    suppListTabs = [
        { id: 1, title: 'Production', active: true, shortName: 'produc' },
        { id: 2, title: 'Simulation', active: false, shortName: 'simul' },
        { id: 3, title: 'Configuration', active: false, shortName: 'config' }
    ];

    changeState(id, e) {
        // rcw:todo rcw:commented  ramin | combined declaration and if condition - @shreya please check if statement is what you want.
        // $('#' + id).next('ul').toggle();
        // if ($('#' + id).next('ul').is(':visible')) {
        if ($('#' + id).next('ul').toggle().is(':visible')) {
            $('#' + id + ' i:first-child').addClass('open-caret').removeClass('close-caret');
        } else {
            $('#' + id + ' i:first-child').addClass('close-caret').removeClass('open-caret');
        }
        e.stopPropagation();
        e.preventDefault();
    }

    // rcw:comment Shreya |  - converting into html elements for showing bold and italic numeric value
    static htmlEntities(str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    constructor(private suppressionService: SuppressionService,
        private contextMenuService: ContextMenuService, private customerContextRecordsService: CustomerContextRecordsService, private router: Router,private toasterService:ToasterService) {
    };

    // rcw:comment Shreya |  - Changing in the dropdown icon from open to close.
    hasChanges() {
        return this.form.dirty;
    }

    ngOnInit(): void {
        this.getAllSuppressions();
        this.getAllCustomerContextRecords();
    }

    ngAfterViewInit() {
        $('.tab-content').addClass('style-4 expand-div wide-div');
        $('body').css('overflow', '');
    }

    // rcw:comment Shreya | Setting the current selected Tab
    setTabParameters(tabName, index) {
        if (index > -1) {
            for (let i = 0; i < this.suppListTabs.length; i++) {
                this.suppListTabs[i].active = i === index;
            }
        }
        this.selectedTab = tabName;
    }

    // rcw:comment Shreya |  - Open Archive Modal Confirmation Popup
    openArchiveConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.archiveSuppressionDisplay = true;
    }

    // rcw:comment Shreya |  - Open Delete Modal Confirmation Popup
    openDeleteConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.deleteSuppressionDisplay = true;
    }

    // rcw:comment Shreya |  - Open Archive Modal Confirmation Popup
    openCancelConfirmation(item, cancelChanges, index) {
        this.modalContent = _.cloneDeep(item);
        this.cancelChangesFlag = cancelChanges;
        this.popupIndex = index;
        this.cancelSuppressionDisplay = true;
    }

    // rcw:comment Shreya |  - Archiving the suppressions by the clicked Suppression ID
    archiveSuppression() {
        this.isRequesting = true;
        const ctrl = this;
        this.suppressionService.archive(ctrl.modalContent.suppressionId).then(function () {;
             ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression archived successfully');
            // Removing the selected Suppression from the selected tab's Suppression list.
            if (ctrl.selectedTab === 'config') {
                for (let i = 0; i < ctrl.configurationSuppressionList.length; i++) {
                    if (ctrl.configurationSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.configurationSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab === 'simul') {
                for (let i = 0; i < ctrl.simulationSuppressionList.length; i++) {
                    if (ctrl.simulationSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.simulationSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab === 'produc') {
                for (let i = 0; i < ctrl.productionSuppressionList.length; i++) {
                    if (ctrl.productionSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.productionSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }
        }).then(function () {
            if (ctrl.modalContent.suppressionId == ctrl.suppressionObj.suppressionId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new Suppression();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression archive failed' );
                ctrl.modalContent = new Suppression();
            }
            ctrl.isRequesting = false;
        });
        this.archiveSuppressionDisplay = false;
    }

    // rcw:comment Shreya |  - Deleting the Suppression by the clicked Suppression ID
    deleteSuppression() {
        const ctrl = this;
        this.isRequesting = true;
        this.suppressionService.delete(ctrl.modalContent.suppressionId).then(function () {
            ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression deleted successfully');
            if (ctrl.selectedTab === 'config') {
                for (let i = 0; i < ctrl.configurationSuppressionList.length; i++) {
                    if (ctrl.configurationSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.configurationSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab === 'simul') {
                for (let i = 0; i < ctrl.simulationSuppressionList.length; i++) {
                    if (ctrl.simulationSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.simulationSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab === 'produc') {
                for (let i = 0; i < ctrl.productionSuppressionList.length; i++) {
                    if (ctrl.productionSuppressionList[i].suppressionId === ctrl.modalContent.suppressionId) {
                        ctrl.productionSuppressionList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }
        }).then(function () {
            if (ctrl.modalContent.suppressionId == ctrl.suppressionObj.suppressionId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new Suppression();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression deletion failed' );
                ctrl.modalContent = new Suppression();
            }
            ctrl.isRequesting = false;
        });
        this.deleteSuppressionDisplay = false;
    }

    checkCancelChanges() {
        if (this.selectedIndex !== -1) {
            this.openCancelConfirmation(this.suppressionObj, 'changeUsecaseCancel', -1);
        } else {
            // If it is newly created usecase it clears the form
            this.clearObjects();
        }
    }

    // rcw:comment Shreya |  - Resetting the variables,flags and selected row/suppression from the list.
    clearObjects() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.suppressionObj = new Suppression();
        this.filter = { 'group': { 'operator': 'AND', 'rules': [] } };
        if (this.configTable != null && this.configTable.selection != null) {
            delete this.configTable.selection;
        }
        if (this.simulTable != null && this.simulTable.selection != null) {
            delete this.simulTable.selection;
        }
        if (this.prodTable != null && this.prodTable.selection != null) {
            delete this.prodTable.selection;
        }
        this.submittedFlag = false;
        this.selectedIndex = -1;
        this.criteriaExp = '';
        this.cancelSuppressionDisplay = false;
    }

    // rcw:comment Shreya |  - Get All the Suppressions from the server
    getAllSuppressions() {
        this.isRequesting = true;
        const suppressionComp = this;
        this.productionSuppressionList = [];
        this.simulationSuppressionList = [];
        this.configurationSuppressionList = [];
        this.suppressionService.get()
            .then(function (eds) {
                for (const item of eds) {
                    if (item.isInProduction) {
                        suppressionComp.productionSuppressionList.push(item);
                    } else if (item.isInSimulation) {
                        suppressionComp.simulationSuppressionList.push(item);
                    }
                    suppressionComp.configurationSuppressionList.push(item);
                }
            }).then(function () {
                suppressionComp.getAllSuppressionElement();
                suppressionComp.getAllCustomerContextRecords();
                suppressionComp.isRequesting = false;
            });
    }

    // rcw:comment Shreya |  - Generate the suppression library element used for rule builder
    getAllSuppressionElement() {
        this.dataSet = [];
        for (const item of this.configurationSuppressionList) {
            const suppObj = new Generic();
            suppObj.name = item.suppressionName;
            suppObj.id = item.suppressionId;
            suppObj.parenttype = 'Suppressions';
            this.dataSet.push(suppObj);
        }
    }

    // rcw:comment Shreya |  - Get All the CCR fields from the server
    getAllCustomerContextRecords() {
        this.isRequesting = true;
        const suppComp = this;
        this.behavioralDnas = [];
        this.customerContextRecordsService.get().then(function (supp) {
            for (const item of supp) {
                if (suppComp.behavioralDnas.indexOf(item.subGroupName) === -1) {
                    suppComp.behavioralDnas.push(item.subGroupName);
                }
                const ccrObj = new Generic();
                ccrObj.name = item.businessName;
                ccrObj.parenttype = item.subGroupName;
                ccrObj.type = 'BDNA';
                suppComp.dataSet.push(ccrObj);
            }
        }).then(function () {
            suppComp.isRequesting = false;
        }).catch(function () {
            suppComp.isRequesting = false;
        });
    }

    // rcw:comment Shreya | - If user clicks yes on redirecting to add new suppression then clear forms
    cancelAddChanges() {
        if (!this.showUpdateScreen) {
            this.openAddPageFlag = true;
            this.showEnlargedScreen = true;
            this.showUpdateScreen = true;
            $('.crud-div').animate({ height: 'toggle' });
            $('#tools').removeClass('fix-height-385').addClass('h-555');
        }
        this.clearObjects();
        this.showUpdateScreen = true;
    }

    cancelUpdateChanges() {
        console.log("inside", this.popupIndex);
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.suppressionObj = _.cloneDeep(this.modalContent);
        this.filter.group = JSON.parse(this.suppressionObj.criteriaExpression);
        this.criteriaExp = this.computed(this.filter.group);
        this.selectedIndex = this.popupIndex;
        this.showUpdateScreen = true;
        this.eventDetectorFlag = true;
        this.showLargeListFlag = false;
        // To reduce the list area and expand the config space
        $('.expand-div').removeClass('wide-div');
        $('.crud-div').css('display', 'block');
        this.cancelSuppressionDisplay = false;
    }

    // rcw:comment Shreya |  - Cancels the changes on the suppression
    cancelSuppressions() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        if (this.selectedIndex !== -1) {
            if (this.selectedTab === 'config') {
                this.suppressionObj = _.cloneDeep(this.configurationSuppressionList[this.selectedIndex]);
            } else if (this.selectedTab === 'simul') {
                this.suppressionObj = _.cloneDeep(this.simulationSuppressionList[this.selectedIndex]);
            } else if (this.selectedTab === 'produc') {
                this.suppressionObj = _.cloneDeep(this.productionSuppressionList[this.selectedIndex]);
            }
            this.filter.group = JSON.parse(this.suppressionObj.criteriaExpression);
            this.criteriaExp = this.computed(this.filter.group);
            this.cancelSuppressionDisplay = false;
        } else {
            this.clearObjects();
        }
    }

    // rcw:comment Shreya | - Not to allow suppression to click other suppression for update once there are any 
    // changes in the suppression form
    rejectSuppressionTraversalChanges() {
        if (this.selectedTab == "config") {
            delete this.configTable.selection;
            for (let item of this.configurationSuppressionList) {
                if (item.suppressionId == this.suppressionObj.suppressionId) {
                    this.configTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "produc") {
            for (let item of this.productionSuppressionList) {
                if (item.suppressionId == this.suppressionObj.suppressionId) {
                    this.prodTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "simul") {
            for (let item of this.simulationSuppressionList) {
                if (item.suppressionId == this.suppressionObj.suppressionId) {
                    this.simulTable.selection = item;
                    break;
                }
            }
        }
        this.cancelSuppressionDisplay = false;
    }

    // rcw:comment Shreya |  - open update form when selected any suppression
    openUpdateTemplate(supp) {
        const suppression = supp.data;
        let index = -1;
        // Retrieving the index of the suppression by identifying the object from the server suppression list
        if (this.selectedTab === 'config') {
            index = this.configurationSuppressionList.indexOf(suppression);
        } else if (this.selectedTab === 'produc') {
            index = this.productionSuppressionList.indexOf(suppression);
        } else if (this.selectedTab === 'simul') {
            index = this.simulationSuppressionList.indexOf(suppression);
        }
        // If object is present returns the index from the list and populates the suppression data onto the below config space
        if (index !== -1) {
            if (this.form.dirty) {
                this.openCancelConfirmation(suppression, 'changeEditCancel', index);
            } else {
                Object.keys(this.form.form.controls).forEach(control => {
                    this.form.form.controls[control].markAsPristine();
                });
                this.suppressionObj = _.cloneDeep(suppression);
                this.filter.group = JSON.parse(this.suppressionObj.criteriaExpression);
                this.criteriaExp = this.computed(this.filter.group);
                this.selectedIndex = index;
                this.showUpdateScreen = true;
                this.eventDetectorFlag = true;
                this.showLargeListFlag = false;
                // To reduce the list area and expand the config space
                $('.expand-div').removeClass('wide-div');
                $('.crud-div').css('display', 'block');
            }

        }
    }

    // rcw:comment Shreya |  - When user clicks on add button.Clears the config space form and also resets the flags and variable
    openAddPage() {
        if (this.form.dirty) {
            this.openCancelConfirmation(this.suppressionObj, 'cancelAddChanges', -1);
        } else {
            if (!this.showUpdateScreen) {
                this.openAddPageFlag = true;
                this.showEnlargedScreen = true;
                this.showUpdateScreen = true;
                $('.crud-div').animate({ height: 'toggle' });
                $('#tools').removeClass('fix-height-385').addClass('h-555');
            }
            this.clearObjects();
            this.showUpdateScreen = true;
        }
    }

    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    setLargeListFlag() {
        if (this.showLargeListFlag) {
            this.showLargeListFlag = false;
            this.showEnlargedScreen = false;
            this.showUpdateScreen = true;
        } else {
            this.showLargeListFlag = true;
            this.showUpdateScreen = false;
        }
    }

    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    togglePage() {
        // when screen is enlarged set list view large and also add page must be enlarged
        if (this.showEnlargedScreen) {
            this.showEnlargedScreen = false;
            this.openAddPageFlag = false;
            $('#tools').removeClass('h-555').addClass('fix-height-385');
        } else {
            this.showEnlargedScreen = true;
            $('#tools').removeClass('fix-height-385').addClass('h-555');
        }
        $('.expand-div').removeClass('wide-div');
    }

    // rcw:comment Shreya |  - On selecting new rule or changing the rule from rule builder
    ongroupChanged(val) {
        this.filter.group = _.cloneDeep(val);
        this.criteriaExp = this.computed(this.filter.group);
        this.suppressionObj.criteriaExpression = JSON.stringify(this.filter.group);
    }

    // rcw:comment Shreya |  - Generating the rule expression from the rule json
    computed(group) {
        let str = '(';
        if (!group) {
            return '';
        }
        for (let i = 0; i < group.rules.length; i++) {
            // rcw:todo please change second line to if statement
            i > 0 && (str += ' <strong>' + group.operator + '</strong> ');
            str += group.rules[i].group ?
                this.computed(group.rules[i].group) :
                group.rules[i].field + ' ' + SuppressionComponent.htmlEntities(group.rules[i].condition) + ' ' + group.rules[i].data;
        }

        return str + ')';
    }

    // rcw:comment Shreya |  - Add/update Suppressions if it satisfies all the validation
    onSubmit(suppressionForm) {
        this.submittedFlag = true;
        if (suppressionForm.form.valid &&
            (this.suppressionObj.criteriaExpression
                && this.suppressionObj.criteriaExpression !== null
                && this.suppressionObj.criteriaExpression !== '')
        ) {
            this.isRequesting = true;
            this.submittedFlag = false;
            if (this.suppressionObj.criteriaExpression && this.suppressionObj.criteriaExpression != null) {
                this.suppressionObj.criteriaExpression = this.suppressionObj.criteriaExpression.replace(/<\/?[^>]+(>|$)/g, '');
            }
            this.suppressionObj.isNestedSuppressionRule = false;
            this.addOrUpdateSuppression();
        }
    }

    //Add or update suppressions using suppression object and form validation
    addOrUpdateSuppression() {
        // If it contains Suppression ID then perform update of Suppression.
        if (this.suppressionObj.suppressionId) {
            delete this.suppressionObj['_$visited'];
            const ctrl = this;
            this.suppressionService.update(this.suppressionObj).then(function (updateSupp) {
                ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression updated successfully');
                if (ctrl.selectedIndex === -1) {
                    ctrl.getAllSuppressions();
                } else {
                    // Update the selected Suppressions at the selected index of the selected tab's list
                    if (ctrl.selectedTab === 'config') {
                        for (let i = 0; i < ctrl.configurationSuppressionList.length; i++) {
                            if (ctrl.configurationSuppressionList[i].suppressionId === updateSupp.suppressionId) {
                                ctrl.configurationSuppressionList[i] = updateSupp;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab === 'simul') {
                        for (let i = 0; i < ctrl.simulationSuppressionList.length; i++) {
                            if (ctrl.simulationSuppressionList[i].suppressionId === updateSupp.suppressionId) {
                                ctrl.simulationSuppressionList[i] = updateSupp;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab === 'produc') {
                        for (let i = 0; i < ctrl.productionSuppressionList.length; i++) {
                            if (ctrl.productionSuppressionList[i].suppressionId === updateSupp.suppressionId) {
                                ctrl.productionSuppressionList[i] = updateSupp;
                                break;
                            }
                        }
                    }
                }
            }).then(function () {
                Object.keys(ctrl.form.form.controls).forEach(control => {
                    ctrl.form.form.controls[control].markAsPristine();
                });
                ctrl.isRequesting = false;
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
                } else {
                    ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression update failed' );
                }
                ctrl.isRequesting = false;
            });
        } else {
            // If it doesn't contains the suppression ID it means its a newly created suppression so perform add operation
            this.suppressionObj.suppressionId = null;
            const ctrl = this;
            this.suppressionService.add(this.suppressionObj).then(function (addedSupp) {
               ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression created successfully');
                if (ctrl.selectedTab === 'config') {
                    ctrl.configurationSuppressionList.push(addedSupp);
                } else if (ctrl.selectedTab === 'simul') {
                    ctrl.simulationSuppressionList.push(addedSupp);
                } else if (ctrl.selectedTab === 'produc') {
                    ctrl.productionSuppressionList.push(addedSupp);
                }
            }).then(function () {
                ctrl.clearObjects();
                ctrl.isRequesting = false;
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
                } else {
                    ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression creation failed' );
                }
                ctrl.isRequesting = false;
            });

        }
    }

    // rcw:comment Shreya |  - This method is for save and publish the Suppressions after all the validations
    onPublish(suppressionForm) {
        this.submittedFlag = true;
        if (suppressionForm.form.valid &&
            (this.suppressionObj.criteriaExpression
                && this.suppressionObj.criteriaExpression !== null
                && this.suppressionObj.criteriaExpression !== '')
        ) {
            this.submittedFlag = false;
            this.isRequesting = true;
            if (this.suppressionObj.criteriaExpression && this.suppressionObj.criteriaExpression != null) {
                this.suppressionObj.criteriaExpression = this.suppressionObj.criteriaExpression.replace(/<\/?[^>]+(>|$)/g, '');
            }
            this.suppressionObj.isNestedSuppressionRule = false;
            this.publishSuppression();
        }
    }

    //rcw:comment Shreya | -Publish the Suppression calling backend API
    publishSuppression() {
        delete this.suppressionObj['_$visited'];
        const ctrl = this;
        this.suppressionService.publish(this.suppressionObj).then(function (publishedSupp) {
           ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression published successfully');
            // If it is the newly created suppression adding that suppression in the selected tab's list
            if (ctrl.suppressionObj.suppressionId == null) {
                if (ctrl.selectedTab === 'config') {
                    ctrl.configurationSuppressionList.push(publishedSupp);
                } else if (ctrl.selectedTab === 'simul') {
                    ctrl.simulationSuppressionList.push(publishedSupp);
                } else if (ctrl.selectedTab === 'produc') {
                    ctrl.productionSuppressionList.push(publishedSupp);
                }
            } else {
                // updating the selected suppression with the updated status and changes in the selected tab's list
                if (ctrl.selectedIndex === -1) {
                    ctrl.getAllSuppressions();
                } else {
                    if (ctrl.selectedTab === 'config') {
                        for (let i = 0; i < ctrl.configurationSuppressionList.length; i++) {
                            if (ctrl.configurationSuppressionList[i].suppressionId === publishedSupp.suppressionId) {
                                ctrl.configurationSuppressionList[i] = publishedSupp;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab === 'simul') {
                        for (let i = 0; i < ctrl.simulationSuppressionList.length; i++) {
                            if (ctrl.simulationSuppressionList[i].suppressionId === publishedSupp.suppressionId) {
                                ctrl.simulationSuppressionList[i] = publishedSupp;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab === 'produc') {
                        for (let i = 0; i < ctrl.productionSuppressionList.length; i++) {
                            if (ctrl.productionSuppressionList[i].suppressionId === publishedSupp.suppressionId) {
                                ctrl.productionSuppressionList[i] = publishedSupp;
                                break;
                            }
                        }
                    }
                }
            }
        }).then(function () {
            Object.keys(ctrl.form.form.controls).forEach(control => {
                ctrl.form.form.controls[control].markAsPristine();
            });
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression publishing failed' );
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya | -Generate next version of selected Suppression and call backend API
    generateNextVersion(suppression: Suppression) {
        const ctrl = this;
        this.isRequesting = true;
        this.suppressionService.nextVersion(suppression.suppressionId).then(function (newVersionSupp) {
            ctrl.toasterService.sendToaster('success', 'Suppression', 'New Version created successfully');
            if (ctrl.selectedTab === 'config') {
                ctrl.configurationSuppressionList.push(newVersionSupp);
            } else if (ctrl.selectedTab === 'simul') {
                ctrl.simulationSuppressionList.push(newVersionSupp);
            } else if (ctrl.selectedTab === 'produc') {
                ctrl.productionSuppressionList.push(newVersionSupp);
            }
        }).then(function () {
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Suppression', 'New version creation failed' );
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya | -Duplicate selected Suppression and call backend API
    duplicateSuppression(suppression: Suppression) {
        const ctrl = this;
        this.isRequesting = true;
        this.suppressionService.copy(suppression.suppressionId).then(function (duplicateSupp) {
            ctrl.toasterService.sendToaster('success', 'Suppression', 'Suppression duplicated successfully');
            if (ctrl.selectedTab === 'config') {
                ctrl.configurationSuppressionList.push(duplicateSupp);
            } else if (ctrl.selectedTab === 'simul') {
                ctrl.simulationSuppressionList.push(duplicateSupp);
            } else if (ctrl.selectedTab === 'produc') {
                ctrl.productionSuppressionList.push(duplicateSupp);
            }
        }).then(function () {
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Suppression', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Suppression', 'Suppression duplication failed' );
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Generates the dynamic context menu for the right click
    // rcw:comment continue | on the suppression name for the various actions like new version,Delete,Archive,Duplicate
    public onContextMenu($event: MouseEvent, item: any): void {
        this.configMenuOptions = [];
        this.configMenuOptions.push({
            html: () => 'Version',
            click: (inner_item) => {
                this.generateNextVersion(inner_item);
            },
            enabled: (inner_item): boolean => {
                return inner_item.versionable;
            }
        });
        this.configMenuOptions.push({
            html: () => 'Duplicate',
            click: (inner_item) => {
                this.duplicateSuppression(inner_item);
            }, enabled: (inner_item): boolean => {
                return inner_item.copyable;
            }
        });
        this.configMenuOptions.push({
            html: () => 'Archive',
            click: (inner_item) => {
                this.openArchiveConfirmation(inner_item);
            }, enabled: (inner_item): boolean => {
                return inner_item.archivable;
            }
        });
        this.configMenuOptions.push({
            html: () => 'Delete',
            click: (inner_item) => {
                this.openDeleteConfirmation(inner_item);
            }, enabled: (inner_item): boolean => {
                return inner_item.deletable;
            }
        });
        this.contextMenuService.show.next({
            actions: this.configMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }

}
