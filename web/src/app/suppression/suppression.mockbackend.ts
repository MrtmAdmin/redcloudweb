import {BaseRequestOptions, Response, ResponseOptions, RequestMethod,Http} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {SuppressionMockDataService} from '../suppression/suppression.mockdata.service';
import {HttpService} from '../services/http.service';

export let SuppressionMockBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HttpService,
    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            let suppressionMockDataService = new SuppressionMockDataService();
            // rcw:comment Shreya | Get All the suppressions
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Get) {
                let suppressionList = suppressionMockDataService.getAllSuppression();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppressionList})
                ));
            }
            // rcw:comment Shreya | Add Suppression
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Post) {
                let suppression = JSON.parse(connection.request.getBody());
                suppression.suppressionId = 4;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            // rcw:comment Shreya | Update Suppression
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Put) {
                let suppression = JSON.parse(connection.request.getBody());
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            // rcw:comment Shreya | Publish Suppression
            if (connection.request.url.endsWith('ered/publish/suppressionrules/') && connection.request.method === RequestMethod.Post) {
                let suppression = JSON.parse(connection.request.getBody());
                suppression.status = "Published";
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            // rcw:comment Shreya | Delete Suppression
            if (connection.request.url.includes('/ered/suppressionrules/') && connection.request.method === RequestMethod.Delete) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            // rcw:comment Shreya | Archive Suppression
            if (connection.request.url.includes('ered/archive/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            // rcw:comment Shreya | Add New Version Suppression
            if (connection.request.url.includes('ered/version/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                let suppresionList = suppressionMockDataService.getAllSuppression();
                suppresionList[suppresionList.length - 1].version = suppresionList[suppresionList.length - 1].version + 0.1;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppresionList[suppresionList.length - 1]})
                ));
            }
            // rcw:comment Shreya | Duplicate Suppression
            if (connection.request.url.includes('ered/copy/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                let suppresionList = suppressionMockDataService.getAllSuppression();
                suppresionList[suppresionList.length - 1].suppressionName = "Duplicate of Suppression - " + suppresionList[suppresionList.length - 1].suppressionName;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppresionList[suppresionList.length - 1]})
                ));
            }
        });

        return new Http(backend, options);
    },
    deps: [MockBackend, BaseRequestOptions]
};