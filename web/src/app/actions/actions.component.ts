/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Generic } from '../models/generic';

@Component({
    moduleId: module.id,
    selector: 'actions',
    templateUrl: 'actions.component.html',
})
export class ActionsComponent implements OnInit, AfterViewInit {

    dataSet: Generic[] = [];
    channelList: Generic[] = [];
    commsList: Generic[] = [];

    constructor(
        private router: Router, ) { };

    ngOnInit(): void {
        this.dataSet = [];
        let datas = new Generic();
        datas.name = "STORED QRA 1";
        datas.id = 1;
        datas.parenttype = "storedQra";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "STORED QRA 2";
        datas.id = 2;
        datas.parenttype = "storedQra";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "STORED QRA 3";
        datas.id = 3;
        datas.parenttype = "storedQra";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "STORED QRA 4";
        datas.id = 4;
        datas.parenttype = "storedQra";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "CHANNEL_EXAMPLE_1";
        datas.id = 1;
        datas.parenttype = "channel";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "CHANNEL_EXAMPLE_2";
        datas.id = 2;
        datas.parenttype = "channel";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "CHANNEL_EXAMPLE_3";
        datas.id = 3;
        datas.parenttype = "channel";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "COMMS_EXAMPLE_1";
        datas.id = 1;
        datas.parenttype = "comms";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "COMMS_EXAMPLE_2";
        datas.id = 2;
        datas.parenttype = "comms";
        this.dataSet.push(datas);
        datas = new Generic();
        datas.name = "COMMS_EXAMPLE_3";
        datas.id = 3;
        datas.parenttype = "comms";
        this.dataSet.push(datas);
    }

    ngAfterViewInit() {
        $('body').css('overflow', '');
    }

    transferChannelsDataSuccess(data) {
        this.channelList.push(data);
    }

    transferCommsDataSuccess(data) {
        this.commsList.push(data);
    }
}

