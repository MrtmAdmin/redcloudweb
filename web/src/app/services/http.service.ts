import { Injectable } from '@angular/core';
import {
    Http,
    ConnectionBackend,
    RequestOptions,
    RequestOptionsArgs,
    RequestMethod,
    Request,
    Response,
    Headers
} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs';
import 'rxjs/Rx';
import { AuthService } from '../login/authentication.service';
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';
import { Configuration } from '../services/configuration';

@Injectable()
export class HttpService extends Http {

    public authService = new AuthService(this, null, null);
    public common = new Common();
    public constant = new Constant();
    private isRefreshing: boolean = false;
    protected refresherStream: Subject<boolean> = new Subject<boolean>();
    public refreshTokenStream: Subject<string> = new Subject<string>();
    public refreshUrls: any[] = [];
    private configuration = new Configuration();

    constructor(backend: ConnectionBackend,
        defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    /**
     * Performs any type of http request.
     * @param url
     * @param options
     * @returns {Observable<Response>}
     */
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        let headers = this.requestInterceptor(options);
        return super.request(url, this.requestOptions(headers)).do((res: Response) => {
            return Observable.of(res);
        }).do((res: Response) => {
            this.onSubscribeSuccess(res);
        }, (error: any) => {
            this.onSubscribeError(error);
        }).finally(() => {
            this.onFinally();
        });
    }

    /**
     * Performs a request with `get` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */
    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        let headers = this.requestInterceptor(options);
        let ctrl = this;
        return super.get(this.getFullUrl(url), this.requestOptions(headers))
            .catch(function (error: any, options?: RequestOptionsArgs): Observable<any> {
                return ctrl.onCatch(error, RequestMethod.Get, ctrl.requestOptions(headers));
            })
            .do((res: Response) => {
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    getLocal(url: string, options?: RequestOptionsArgs): Observable<any> {
        return super.get(url, options);
    }

    /**
     * Performs a request with `post` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */
    post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        let headers = this.requestInterceptor(options);
        let ctrl = this;
        return super.post(this.getFullUrl(url), body, this.requestOptions(headers))
            .catch(function (error): Observable<any> {
                return ctrl.onCatch(error, RequestMethod.Post, ctrl.requestOptions(headers));
            })
            .do((res: Response) => {
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Performs a request with `put` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */
    put(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        let headers = this.requestInterceptor(options);
        let ctrl = this;
        return super.put(this.getFullUrl(url), body, this.requestOptions(headers))
            .catch(function (error): Observable<any> {
                return ctrl.onCatch(error, RequestMethod.Put, ctrl.requestOptions(headers));
            })
            .do((res: Response) => {
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Performs a request with `delete` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */
    delete(url: string, options?: RequestOptionsArgs): Observable<any> {
        let headers = this.requestInterceptor(options);
        let ctrl = this;
        return super.delete(this.getFullUrl(url), headers)
            .catch(function (error): Observable<any> {
                return ctrl.onCatch(error, RequestMethod.Delete, ctrl.requestOptions(headers));
            })
            .do((res: Response) => {
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }


    /**
     * Request options.
     * @param options
     * @returns {RequestOptionsArgs}
     */
    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        return options;
    }

    /**
     * Build API url.
     * @param url
     * @returns {string}
     */
    private getFullUrl(url: string): string {
        // return full URL to API here
        return url;
    }

    /**
     * Request interceptor.
     */
    private requestInterceptor(options: RequestOptionsArgs): RequestOptionsArgs {
        // add authorization header with oauth2 token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.common.getAccessToken() });
        if (options == null || options == undefined) {
            options = new RequestOptions({ headers: headers });
        } else {
            options.headers = headers;
        }
        return options;
    }

    /**
     * Response interceptor.
     */
    private responseInterceptor(): void {
        //        this.loaderService.hidePreloader();
    }

    /**
     * Error handler.
     * @param error
     * @param caught
     * @returns {ErrorObservable}
     */
    private onCatch(error: any, method: RequestMethod, options?: RequestOptionsArgs): Observable<any> {
        let ctrl = this;
        if (error.status == 401 && !(_.endsWith(error['url'], 'auth/user/login'))) {
            return this.refreshTheToken(error, method, options).flatMap(
                result => {
                    const reqOptions = new RequestOptions({
                        method: method,
                        headers: new Headers({ 'Authorization': 'Bearer ' + ctrl.common.getAccessToken() })
                    });
                    const req = new Request(reqOptions.merge({
                        url: error['url'],
                    }));
                    return ctrl.request(req, options).flatMap(
                        result => {
                            return Observable.of(result);
                        });
                });

        } else {
            return Observable.throw(error);
        }
    }

    public refreshTheToken(error, method: RequestMethod, options?: RequestOptionsArgs): Observable<any> {
        //        this.refreshUrls.push(error['url']);
        // if is refreshing token, wait next false value
        if (this.isRefreshing) {
            return Observable.create((observer: Observer<void>) => {
                this.refresherStream
                    .subscribe((value) => {
                        if (!value) {
                            observer.next(null);
                            observer.complete();
                        }
                    });
            });
        } else {
            return this._refreshTheToken(error, method, options);
        }
    }

    private emitRefreshToken() {
        const refreshToken = localStorage.getItem('refresh_token');
        this.refreshTokenStream.next(refreshToken);
    }

    private setRefreshing(value: boolean) {
        this.isRefreshing = value;
        this.refresherStream.next(this.isRefreshing);
    }

    protected _refreshTheToken(error, method: RequestMethod, options?: RequestOptionsArgs) {
        this.setRefreshing(true);
        let ctrl = this;
        return this.updateToken(error, options).flatMap((result: boolean) => {
            //if got new access token - retry request
            if (result) {
                const reqOptions = new RequestOptions({
                    method: method,
                    headers: new Headers({'Authorization': 'Bearer ' + ctrl.common.getAccessToken()})
                });
                const req = new Request(reqOptions.merge({
                    url: error['url'],
                }));

                return ctrl.request(req, options);
            }
            //otherwise - throw error
            else {
                return Observable.throw(new Error('Can\'t refresh the token'));
            }

        });

    }

    // rcw:comment Shreya | Get the new access token from backend by sending the new refresh token
    updateToken(error: any, options?: RequestOptionsArgs): Observable<any> {
        let ctrl = this;
        return this.get(this.configuration.ServerWithApiUrl + "auth/refreshtoken?refresh_token=" + this.common.getRefreshToken())
            .map((response: Response) => {
                var returnedBody: any = response.json();
                returnedBody = returnedBody['details'];
                if (typeof returnedBody['access_token'] !== 'undefined') {
                    localStorage.setItem(ctrl.constant.tokenName, returnedBody['access_token']);
                    localStorage.setItem(ctrl.constant.refreshToken, returnedBody['refresh_token']);
                    ctrl.setRefreshing(false);
                    ctrl.emitRefreshToken();
                    return Observable.of(true);
                }
                else {
                    return Observable.of(false);
                }
            }).catch(function (error) {
                ctrl.setRefreshing(false);
                ctrl.emitRefreshToken();
                localStorage.clear();
                ctrl.common.deleteAllCookies();
                window.location.href = "/#/error?action=sessionTimeout";
                //ctrl.authService.goToErrorPageDueToSessionTimeout();
                //location.reload(true);
                return Observable.throw(error);
            })
    }

    /**
     * onSubscribeSuccess
     * @param res
     */
    private onSubscribeSuccess(res: Response): void {
    }

    /**
     * onSubscribeError
     * @param error
     */
    private onSubscribeError(error: any): void {
    }

    /**
     * onFinally
     */
    private onFinally(): void {
        this.responseInterceptor();
    }
}
