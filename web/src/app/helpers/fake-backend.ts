import {BaseRequestOptions, Response, ResponseOptions, RequestMethod,Http} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {SuppressionMockDataService} from '../suppression/suppression.mockdata.service';
import {EventDetectorMockDataService} from '../eventdetector/eventdetector.mockdata.service';

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            let testUser = {username: 'test', password: 'test', firstName: 'Test', lastName: 'User'};
            let suppressionMockDataService = new SuppressionMockDataService();
            let eventDetectorMockDataService = new EventDetectorMockDataService();
            // fake authenticate api end point
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                // get parameters from post request
                let params = JSON.parse(connection.request.getBody());

                // check user credentials and return fake jwt token if valid
                if (params.username === testUser.username && params.password === testUser.password) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200, body: {token: 'fake-jwt-token'}})
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200})
                    ));
                }
            }
            if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 200, body: [testUser]})
                    ));
                } else {
                    // return 401 not authorised if token is null or invalid
                    connection.mockRespond(new Response(
                        new ResponseOptions({status: 401})
                    ));
                }
            }
            if (connection.request.url.endsWith('ered/page/eventdetectors?pageIndex=0&pageSize=2000') && connection.request.method === RequestMethod.Get) {
                let eventDetectors = eventDetectorMockDataService.getAllEventDetector();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: eventDetectors})
                ));
            }
            if (connection.request.url.endsWith('ered/eventdetectors') && connection.request.method === RequestMethod.Post) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                newEventDetector.eventDetectorId = 7;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.endsWith('ered/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.endsWith('ered/publish/eventdetectors/') && connection.request.method === RequestMethod.Post) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                newEventDetector.status = "Published";
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.includes('ered/eventdetectors/') && connection.request.method === RequestMethod.Delete) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/archive/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/version/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let edList = eventDetectorMockDataService.getAllEventDetector();
                edList[edList.length - 1].version = edList[edList.length - 1].version + 0.1;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:edList[edList.length - 1]})
                ));
            }
            if (connection.request.url.includes('ered/copy/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let edList = eventDetectorMockDataService.getAllEventDetector();
                edList[edList.length - 1].eventName = "Duplicate of Event Detector - " + edList[edList.length - 1].eventName;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:edList[edList.length - 1]})
                ));
            }
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Get) {
                let suppressionList = suppressionMockDataService.getAllSuppression();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppressionList})
                ));
            }
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Post) {
                let suppression = JSON.parse(connection.request.getBody());
                suppression.suppressionId = 4;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            if (connection.request.url.endsWith('/ered/suppressionrules') && connection.request.method === RequestMethod.Put) {
                let suppression = JSON.parse(connection.request.getBody());
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            if (connection.request.url.endsWith('ered/publish/suppressionrules/') && connection.request.method === RequestMethod.Post) {
                let suppression = JSON.parse(connection.request.getBody());
                suppression.status = "Published";
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: suppression})
                ));
            }
            if (connection.request.url.includes('/ered/suppressionrules/') && connection.request.method === RequestMethod.Delete) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/archive/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/version/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                let suppresionList = suppressionMockDataService.getAllSuppression();
                suppresionList[suppresionList.length - 1].version = suppresionList[suppresionList.length - 1].version + 0.1;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:suppresionList[suppresionList.length - 1]})
                ));
            }
            if (connection.request.url.includes('ered/copy/suppressionrules/') && connection.request.method === RequestMethod.Put) {
                let suppresionList = suppressionMockDataService.getAllSuppression();
                suppresionList[suppresionList.length - 1].suppressionName = "Duplicate of Suppression - " +suppresionList[suppresionList.length - 1].suppressionName;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:suppresionList[suppresionList.length - 1]})
                ));
            }
        });

        return new Http(backend, options);
    },
    deps: [MockBackend, BaseRequestOptions]
};