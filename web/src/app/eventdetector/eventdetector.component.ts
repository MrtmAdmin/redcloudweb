/*
 * This is the Event Detector component which
 * binds the suppression HTML with the Event Detector service.
 * All the logic related to Event Detector are mentioned in this component file.
 * @Author : Shreya
 */

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventDetectorService } from './eventdetector.service';
import { SuppressionService } from '../suppression/suppression.service';
import { FeedsService } from '../feeds/feeds.service';
import { ConsumerService } from '../consumer/consumer.service';
import { CustomerContextRecordsService } from '../customercontextrecords/customer-context-records.service';
import { EventDetector } from '../eventdetector/eventdetector';
import { ExclusionDate } from '../eventdetector/eventdetector';
import { Generic } from '../models/generic';
import { TimeWindow } from "../models/timeWindow";
import { Suppression } from "../suppression/suppression";
import { SuppressionAsnBo } from "../suppression/suppression";
import * as moment from 'moment/moment';
import { ContextMenuService } from '../directives/angular2-contextmenu/src/contextMenu.service';
import { DataTable } from 'primeng/components/datatable/datatable';
import * as _ from "lodash";
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import { ToasterService } from '../services/toaster.service';

export enum DayMapper {
    M = 1, Tu, W, Th, F, Sa, Su
}

@Component({
    moduleId: module.id,
    selector: 'event-detectors',
    templateUrl: 'eventdetector.component.html',
})
export class EventDetectorComponent implements OnInit, AfterViewInit {

    @ViewChild('configTable')
    configTable: DataTable;

    @ViewChild('simulTable')
    simulTable: DataTable;

    @ViewChild('prodTable')
    prodTable: DataTable;

    @ViewChild('eventDetectorForm') form;

    DayMapper: typeof DayMapper = DayMapper;


    productionEventDetectorList: EventDetector[] = [];
    simulationEventDetectorList: EventDetector[] = [];
    configurationEventDetectorList: EventDetector[] = [];
    prodSuppSearchList: string[] = [];
    simulSuppSearchList: string[] = [];
    configSuppSearchList: string[] = [];
    finalProductionEventDetectorList: EventDetector[] = [];
    finalSimulationEventDetectorList: EventDetector[] = [];
    finalConfigurationEventDetectorList: EventDetector[] = [];
    eventDetectorObj = new EventDetector();
    timingObjList: TimeWindow[] = [];
    showUpdateScreen: boolean = false;
    showEnlargedScreen: boolean = false;
    eventDetectorFlag: boolean = false;
    openAddPageFlag: boolean = false;
    showLargeListFlag: boolean = true;
    testHeightFlag: boolean = false;
    submittedFlag: boolean = false;
    timingValidFlag: boolean = true;
    criteriaExp: string = "";
    field: any;
    timeIndex: number = 0;
    dataSet: Generic[] = [];
    weekDays: any = [{ id: 0, day: "Monday" }, { id: 1, day: "Tuesday" }, { id: 2, day: "Wednesday" },
    { id: 3, day: "Thursday" }, { id: 4, day: "Friday" }, { id: 5, day: "Saturday" }, { id: 6, day: "Sunday" }];
    weekSmDays: any = [{ id: 0, day: "M" }, { id: 1, day: "Tu" }, { id: 2, day: "W" },
    { id: 3, day: "Th" }, { id: 4, day: "F" }, { id: 5, day: "Sa" }, { id: 6, day: "Su" }];
    excludeDates: ExclusionDate[] = [];
    filter = { "group": { "operator": "AND", "rules": [] } };
    configSearch: string = "";
    upperInstance: number = 0;
    belowInstance: number = 0;
    selectedConfigSupp: string = "";
    selectedSimulSupp: string = "";
    selectedProdSupp: string = "";
    selectedTab: string = "produc";
    configMenuOptions = [];
    modalContent = new EventDetector();
    selectedIndex: any = -1;
    excludeDateVal: any;
    isRequesting: boolean;
    behavioralDnas = [];
    archiveEdDisplay: boolean = false;
    deleteEdDisplay: boolean = false;
    cancelEdDisplay: boolean = false;
    cancelChangesFlag: string = '';
    ruleBuilderEdDisplay: boolean = false;
    popupIndex: number = -1;
    dirtyFlag: boolean = false;
    eventListTabs = [{ id: 1, title: "Production", active: true, shortName: "produc" }, { id: 2, title: "Simulation", active: false, shortName: "simul" }, { id: 3, title: "Configuration", active: false, shortName: "config" }];
    toolsListTabs = [{ id: 1, title: "Events", active: true }, { id: 2, title: "Suppressions", active: false }, { id: 3, title: "Consumers", active: false }];
    updateAccess: boolean = false;

    constructor(private eventDetectorService: EventDetectorService,
        private contextMenuService: ContextMenuService,
        private suppressionService: SuppressionService,
        private feedsService: FeedsService, private consumerService: ConsumerService,
        private customerContextRecordsService: CustomerContextRecordsService,
        private common: Common,
        private constant: Constant,
        private router: Router,
        private toasterService: ToasterService) { };
    // rcw:comment Shreya |  - To check whether form has changed or not
    hasChanges() {
        return this.form.dirty || this.dirtyFlag;
    }

    ngOnInit(): void {
        this.getAllEventDetectors();
        this.getAllSuppressionRules();
        this.getAllFeeds();
        this.getAllConsumers();
        this.getAllCustomerContextRecords();
        this.generateTimingObj();
        this.updateAccess = this.common.checkUpdateAccess('EVENT_DETECTOR');

    }

    ngAfterViewInit() {
        $('body').css('overflow', '');
        var comp = this;
        /*Whenever any suppression is selected from the list, registering the scroll event of the table
         * and whenever user scrolls calculate the count of the number of occurence of the suppression
        */

        this.changeSizeFromViewPort();
        $("#tabContentMainDiv").scroll(function () {
            clearTimeout($.data(this, 'scrollTimer'));
            $.data(this, 'scrollTimer', setTimeout(function () {
                var container = document.getElementById("tabContentMainDiv"),
                    // Calculating the tr of the table.
                    tr = $("#" + comp.selectedTab + ' .supp-selected'),
                    visible = [],
                    below = [],
                    upper = [],
                    i, j, cur;
                // Div's area and dimensions
                var parRect = container.getBoundingClientRect();
                // Looping to the trs
                for (i = 0, j = tr.length; i < j; i++) {
                    cur = tr[i];
                    //Retrieving the cursor click event and postion i.e dimensions
                    var elRect = cur.getBoundingClientRect();
                    //If clicked top is greater than div's top and same for bottom that means it is visible in the current screen
                    if (elRect.top >= parRect.top &&
                        elRect.bottom <= parRect.bottom) {
                        visible.push(cur.id);
                    } else if (elRect.top < parRect.top) {
                        //If top is less than the screen top it means it is above the scroll position so consider it to be above
                        upper.push(cur.id);
                    } else if (elRect.top > parRect.bottom) {
                        //If top is less than the screen top it means it is below the scroll position so consider it to be below
                        below.push(cur.id);
                    }
                }
                //Assigning the upper/below suppression count
                comp.upperInstance = upper.length;
                comp.belowInstance = below.length;
                //Following the pagination if the suppression is on next page
                //If selected Tab is config then calculate this from config list
                if (comp.selectedTab == "config") {
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.configurationEventDetectorList.forEach((item, index) => {
                        /*Ignore the list showed currently on the screen and who doesn't contains
                        the suppressions and calculate the above suppressions in the list frame*/
                        if (index < comp.configTable.first && item.suppressionRule != null) {
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    //Increase the count if found in other pages along client side pagination
                                    comp.upperInstance = comp.upperInstance + 1;
                                }
                            }
                        }
                    });
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.configurationEventDetectorList.forEach((item, index) => {
                        /*Ignore the list showed currently on the screen and who doesn't contains
                        the suppressions and calculate the below suppressions in the list frame*/
                        if (index >= (comp.configTable.first + comp.configTable.rows) && item.suppressionRule != null) {
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    comp.belowInstance = comp.belowInstance + 1;
                                }
                            }
                        }
                    });
                } else if (comp.selectedTab == "produc") {
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.productionEventDetectorList.forEach((item, index) => {
                        if (index < comp.prodTable.first && item.suppressionRule != null) {
                            /*Ignore the list showed currently on the screen and who doesn't contains
                       the suppressions and calculate the above suppressions in the list frame*/
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    comp.upperInstance = comp.upperInstance + 1;
                                }
                            }
                        }
                    });
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.productionEventDetectorList.forEach((item, index) => {
                        /*Ignore the list showed currently on the screen and who doesn't contains
                        the suppressions and calculate the below suppressions in the list frame*/
                        if (index >= (comp.prodTable.first + comp.prodTable.rows) && item.suppressionRule != null) {
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    comp.belowInstance = comp.belowInstance + 1;
                                }
                            }
                        }
                    });
                } else if (comp.selectedTab == "simul") {
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.simulationEventDetectorList.forEach((item, index) => {
                        /*Ignore the list showed currently on the screen and who doesn't contains
                      the suppressions and calculate the above suppressions in the list frame*/
                        if (index < comp.simulTable.first && item.suppressionRule != null) {
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    comp.upperInstance = comp.upperInstance + 1;
                                }
                            }
                        }
                    });
                    //Looping throught the list and ignoring the viewable suppressions on the list
                    comp.simulationEventDetectorList.forEach((item, index) => {
                        /*Ignore the list showed currently on the screen and who doesn't contains
                        the suppressions and calculate the below suppressions in the list frame*/
                        if (index >= (comp.simulTable.first + comp.simulTable.rows) && item.suppressionRule != null) {
                            for (let supp of item.suppressionRule) {
                                if (supp.suppressionName == comp.selectedConfigSupp) {
                                    comp.belowInstance = comp.belowInstance + 1;
                                }
                            }
                        }
                    });
                }
                //As long as scroll event is fired it will display the popup container
                $(".speech").show();
                // rcw:deleted | to typescript type error
                // var container = $(".speech");
                // container.show();
            }, 250));
        });
    }
    // sets height dynamically 
    changeSizeFromViewPort() {
        if (!this.testHeightFlag) {
            var panelGot = this.common.winResize('menuE', 'listTab');
            $('#tabContentMainDiv').css('height', panelGot - 20);
            $('#tools').css('height', panelGot - 20);
            $('#enlargedEventDiv').css('height', panelGot - 20);
            $('#eventDetConfig').css('height', panelGot - 20);
            $('#suppTab').css('height', panelGot - 20);
            $('#consTab').css('height', panelGot - 20);
        }
        else {
            var panelGot = this.common.winResize('menuE', 'listTab');
            $('#tabContentMainDiv').css('height', (panelGot - 20) / 2 - 20);
            $('#tools').css('height', (panelGot - 20) / 2 - 20);
            $('#enlargedEventDiv').css('height', (panelGot - 20) / 2 - 20);
            $('#eventDetConfig').css('height', (panelGot - 20) / 2 - 20);
        }
    }
    // rcw:comment Shreya |  - Setting the current selected Tab
    setTabParameters(tabName, index) {
        if (index > -1) {
            for (let i = 0; i < this.eventListTabs.length; i++) {
                if (i == index) {
                    this.eventListTabs[i].active = true;
                } else {
                    this.eventListTabs[i].active = false;
                }
            }
        }
        this.selectedTab = tabName;
        this.upperInstance = 0;
        this.belowInstance = 0;
    }

    // rcw:comment Shreya |  - Setting the current selected Tab
    setToolTab(index) {
        if (index > -1) {
            for (let i = 0; i < this.toolsListTabs.length; i++) {
                if (i == index) {
                    this.toolsListTabs[i].active = true;
                } else {
                    this.toolsListTabs[i].active = false;
                }
            }
        }
    }

    // rcw:comment Shreya |  - Open Archive Modal Confirmation Popup
    openArchiveConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.archiveEdDisplay = true;
    }

    // rcw:comment Shreya |  - Open Delete Modal Confirmation Popup
    openDeleteConfirmation(item) {
        this.modalContent = _.cloneDeep(item);
        this.deleteEdDisplay = true;
    }

    // rcw:comment Shreya |  - Open Archive Modal Confirmation Popup
    openCancelConfirmation(item, cancelChanges, index) {
        this.modalContent = _.cloneDeep(item);
        this.cancelChangesFlag = cancelChanges;
        this.popupIndex = index;
        this.cancelEdDisplay = true;
    }

    // rcw:comment Shreya |  - Archiving the event detectors by the clicked EventDetector ID
    archiveEventDetector() {
        var ctrl = this;
        this.isRequesting = true;
        this.eventDetectorService.archive(ctrl.modalContent.eventDetectionId).then(function () {
            ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector archived successfully');
            if (ctrl.selectedTab == "config") {
                for (let i = 0; i < ctrl.configurationEventDetectorList.length; i++) {
                    if (ctrl.configurationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.configurationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalConfigurationEventDetectorList.length; i++) {
                    if (ctrl.finalConfigurationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalConfigurationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "simul") {
                for (let i = 0; i < ctrl.simulationEventDetectorList.length; i++) {
                    if (ctrl.simulationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.simulationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalSimulationEventDetectorList.length; i++) {
                    if (ctrl.finalSimulationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalSimulationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "produc") {
                for (let i = 0; i < ctrl.productionEventDetectorList.length; i++) {
                    if (ctrl.productionEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.productionEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalProductionEventDetectorList.length; i++) {
                    if (ctrl.finalProductionEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalProductionEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }
        }).then(function () {
            if (ctrl.modalContent.eventDetectionId == ctrl.eventDetectorObj.eventDetectionId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new EventDetector();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector archive failed');
                ctrl.modalContent = new EventDetector();
            }
            ctrl.isRequesting = false;
        });
        this.archiveEdDisplay = false;
    }

    // rcw:comment Shreya |  - Deleting the event detectors by the clicked EventDetector ID
    deleteEventDetector() {
        var ctrl = this;
        this.isRequesting = true;
        this.eventDetectorService.delete(ctrl.modalContent.eventDetectionId).then(function () {
            ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector deleted successfully');
            if (ctrl.selectedTab == "config") {
                for (let i = 0; i < ctrl.configurationEventDetectorList.length; i++) {
                    if (ctrl.configurationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.configurationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalConfigurationEventDetectorList.length; i++) {
                    if (ctrl.finalConfigurationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalConfigurationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "simul") {
                for (let i = 0; i < ctrl.simulationEventDetectorList.length; i++) {
                    if (ctrl.simulationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.simulationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalSimulationEventDetectorList.length; i++) {
                    if (ctrl.finalSimulationEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalSimulationEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            } else if (ctrl.selectedTab == "produc") {
                for (let i = 0; i < ctrl.productionEventDetectorList.length; i++) {
                    if (ctrl.productionEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.productionEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
                for (let i = 0; i < ctrl.finalProductionEventDetectorList.length; i++) {
                    if (ctrl.finalProductionEventDetectorList[i].eventDetectionId == ctrl.modalContent.eventDetectionId) {
                        ctrl.finalProductionEventDetectorList.splice(i, 1);
                        i--;
                        break;
                    }
                }
            }
        }).then(function () {
            if (ctrl.modalContent.eventDetectionId == ctrl.eventDetectorObj.eventDetectionId) {
                ctrl.clearObjects();
            }
            ctrl.modalContent = new EventDetector();
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector deletion failed');
                ctrl.modalContent = new EventDetector();
            }
            ctrl.isRequesting = false;
        });
        this.deleteEdDisplay = false;
    }

    // rcw:comment Shreya |  - Resetting the variables,flags and selected row/event detectors from the list.
    clearObjects() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.eventDetectorObj = new EventDetector();
        this.selectedIndex = -1;
        this.excludeDateVal = "";
        this.timingObjList = [];
        this.generateTimingObj();
        this.criteriaExp = "";
        this.filter = { "group": { "operator": "AND", "rules": [] } };
        if (this.configTable != null && this.configTable.selection != null) {
            delete this.configTable.selection;
        }
        if (this.simulTable != null && this.simulTable.selection != null) {
            delete this.simulTable.selection;
        }
        if (this.prodTable != null && this.prodTable.selection != null) {
            delete this.prodTable.selection;
        }
        this.submittedFlag = false;
        this.dirtyFlag = false;
        this.cancelEdDisplay = false;

    }

    // rcw:comment Shreya |  - Get All the Suppressions from the server
    getAllSuppressionRules() {
        this.isRequesting = true;
        var eventDetectorComp = this;
        this.suppressionService.get().then(function (supp) {
            for (let item of supp) {
                let suppObj = new Generic();
                suppObj.name = item.suppressionName;
                suppObj.id = item.suppressionId
                suppObj.parenttype = "Suppressions";
                eventDetectorComp.dataSet.push(suppObj);
            }
        }).then(function () {
            eventDetectorComp.isRequesting = false;
        }).catch(function () {
            eventDetectorComp.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Get All the Feeds from the server
    getAllFeeds() {
        this.isRequesting = true;
        var eventDetectorComp = this;
        this.feedsService.get().then(function (supp) {
            for (let item of supp) {
                let feedObj = new Generic();
                feedObj.name = item.feedName;
                feedObj.parenttype = "Feeds";
                feedObj.type = "REALTIME";
                eventDetectorComp.dataSet.push(feedObj);
            }
        }).then(function () {
            eventDetectorComp.isRequesting = false;
        }).catch(function () {
            eventDetectorComp.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Get All the Consumers from the server
    getAllConsumers() {
        this.isRequesting = true;
        var eventDetectorComp = this;
        this.consumerService.get().then(function (supp) {
            for (let item of supp) {
                let consumerObj = new Generic();
                consumerObj.name = item.consumerName;
                consumerObj.parenttype = "Consumers";
                eventDetectorComp.dataSet.push(consumerObj);
            }
        }).then(function () {
            eventDetectorComp.isRequesting = false;
        }).catch(function () {
            eventDetectorComp.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Get All the CCR Fields from the server
    getAllCustomerContextRecords() {
        this.isRequesting = true;
        var eventDetectorComp = this;
        this.behavioralDnas = [];
        this.customerContextRecordsService.get().then(function (supp) {
            for (let item of supp) {
                if (eventDetectorComp.behavioralDnas.indexOf(item.subGroupName) == -1) {
                    eventDetectorComp.behavioralDnas.push(item.subGroupName);
                }
                let ccrObj = new Generic();
                ccrObj.name = item.businessName;
                ccrObj.parenttype = item.subGroupName;
                ccrObj.type = "BDNA";
                eventDetectorComp.dataSet.push(ccrObj);
            }
        }).then(function () {
            eventDetectorComp.isRequesting = false;
        }).catch(function () {
            eventDetectorComp.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Get All the Event Detector List from the server
    getAllEventDetectors() {
        this.isRequesting = true;
        var eventDetectorComp = this;
        this.productionEventDetectorList = [];
        this.simulationEventDetectorList = [];
        this.configurationEventDetectorList = [];
        this.finalProductionEventDetectorList = [];
        this.finalSimulationEventDetectorList = [];
        this.finalConfigurationEventDetectorList = [];
        this.eventDetectorService.getPaged(0, 2000)
            .then(function (eds) {
                for (let item of eds) {
                    if (item.isInProduction) {
                        eventDetectorComp.productionEventDetectorList.push(item);
                        eventDetectorComp.finalProductionEventDetectorList.push(item);
                    } else if (item.isInSimulation) {
                        eventDetectorComp.simulationEventDetectorList.push(item);
                        eventDetectorComp.finalSimulationEventDetectorList.push(item);
                    }
                    eventDetectorComp.configurationEventDetectorList.push(item)
                    eventDetectorComp.finalConfigurationEventDetectorList.push(item)
                }
            }).then(function () {
                eventDetectorComp.isRequesting = false;
            }).catch(function () {
                eventDetectorComp.isRequesting = false;
            });
    }

    // rcw:comment Shreya | - If user clicks yes on redirecting to add new event detector then clear forms
    cancelAddChanges() {
        if (!this.showUpdateScreen) {
            this.openAddPageFlag = true;
            this.showEnlargedScreen = true;
            this.showUpdateScreen = true;
            $(".crud-div").animate({ height: "toggle" });
            $("#tools").removeClass("fix-height-385").addClass("h-555");
        }
        this.clearObjects();
        this.showUpdateScreen = true;
        this.dirtyFlag = false;
    }

    // rcw:comment Shreya | -If user clicks yes on redirecting to other event detector then change the ed on update page
    cancelUpdateChanges() {
        let eventDetector = _.cloneDeep(this.modalContent);
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.eventDetectorObj = _.cloneDeep(eventDetector);
        this.filter.group = JSON.parse(this.eventDetectorObj.criteriaExpression);
        this.criteriaExp = this.computed(this.filter.group);
        this.timingObjList = [];
        this.generateTimingObj();
        this.selectedIndex = this.popupIndex;
        if (eventDetector.timeWindow != undefined) {
            for (let item of eventDetector.timeWindow) {
                var start = moment(item.startTime, 'HH:mm:ss').format('hh:mm:ss a');
                var end = moment(item.endTime, 'HH:mm:ss').format('hh:mm:ss a');
                var startMeri = moment(item.startTime, 'HH:mm:ss').format('a');
                var endMeri = moment(item.endTime, 'HH:mm:ss').format('a');
                var starttime = start.split(':');
                var endtime = end.split(':');
                let timing = new TimeWindow();
                timing.startHrTime = starttime[0];
                timing.startMinTime = starttime[1];
                timing.startMeridian = startMeri;
                timing.endHrTime = endtime[0];
                timing.endMinTime = endtime[1];
                timing.endMeridian = endMeri;
                timing.timeWindowDay = item.timeWindowDay;
                this.timingObjList[item.timeWindowDay - 1] = timing;
            }
        }
        this.showUpdateScreen = true;
        this.eventDetectorFlag = true;
        this.showLargeListFlag = false;
        $(".expand-div").removeClass("wide-div");
        $(".crud-div").css("display", "block");
        this.dirtyFlag = false;
        this.cancelEdDisplay = false;
    }

    // rcw:comment Shreya | - Not to allow event detector to click other Ed for update once there are any 
    // changes in the ED form
    rejectEdTraversalChanges() {
        if (this.selectedTab == "config") {
            console.log("this.eventDetectorObj :", this.eventDetectorObj);
            delete this.configTable.selection;
            for (let item of this.configurationEventDetectorList) {
                if (item.eventDetectionId == this.eventDetectorObj.eventDetectionId) {
                    this.configTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "produc") {
            for (let item of this.productionEventDetectorList) {
                if (item.eventDetectionId == this.eventDetectorObj.eventDetectionId) {
                    this.prodTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "simul") {
            for (let item of this.simulationEventDetectorList) {
                if (item.eventDetectionId == this.eventDetectorObj.eventDetectionId) {
                    this.simulTable.selection = item;
                    break;
                }
            }
        }
        this.cancelEdDisplay = false;
    }

    // rcw:comment Shreya |  - open update form when selected any Event Detector
    openUpdateTemplate(ed) {
        if (!this.testHeightFlag) {
            this.testHeightFlag = true;
        }

        this.changeSizeFromViewPort();
        let eventDetector = ed.data;
        var index = -1;
        if (this.selectedTab == "config") {
            index = this.configurationEventDetectorList.indexOf(eventDetector);
        } else if (this.selectedTab == "produc") {
            index = this.productionEventDetectorList.indexOf(eventDetector);
        } else if (this.selectedTab == "simul") {
            index = this.simulationEventDetectorList.indexOf(eventDetector);
        }
        console.log("open index :" + index);
        if (index !== -1) {
            if (this.form.dirty || this.dirtyFlag) {
                this.openCancelConfirmation(eventDetector, 'changeEditCancel', index);
            } else {
                Object.keys(this.form.form.controls).forEach(control => {
                    this.form.form.controls[control].markAsPristine();
                });
                this.eventDetectorObj = _.cloneDeep(eventDetector);
                this.filter.group = JSON.parse(this.eventDetectorObj.criteriaExpression);
                this.criteriaExp = this.computed(this.filter.group);
                this.selectedIndex = index;
                this.timingObjList = [];
                this.generateTimingObj();
                if (eventDetector.timeWindow != undefined) {
                    for (let item of eventDetector.timeWindow) {
                        var start = moment(item.startTime, 'HH:mm:ss').format('hh:mm:ss a');
                        var end = moment(item.endTime, 'HH:mm:ss').format('hh:mm:ss a');
                        var startMeri = moment(item.startTime, 'HH:mm:ss').format('a');
                        var endMeri = moment(item.endTime, 'HH:mm:ss').format('a');
                        var starttime = start.split(':');
                        var endtime = end.split(':');
                        let timing = new TimeWindow();
                        timing.startHrTime = starttime[0];
                        timing.startMinTime = starttime[1];
                        timing.startMeridian = startMeri;
                        timing.endHrTime = endtime[0];
                        timing.endMinTime = endtime[1];
                        timing.endMeridian = endMeri;
                        timing.timeWindowDay = item.timeWindowDay;
                        this.timingObjList[item.timeWindowDay - 1] = timing;
                    }
                }
                this.showUpdateScreen = true;
                this.eventDetectorFlag = true;
                this.showLargeListFlag = false;
                $(".expand-div").removeClass("wide-div");
                $(".crud-div").css("display", "block");
                this.dirtyFlag = false;
            }
        }
    }

    // rcw:comment Shreya |  - When user clicks on add button.Clears the config space form and also resets the flags and variable
    openAddPage() {
        if (this.form.dirty || this.dirtyFlag) {
            this.openCancelConfirmation(this.eventDetectorObj, 'cancelAddChanges', -1);
        } else {
            if (!this.showUpdateScreen) {
                this.openAddPageFlag = true;
                this.showEnlargedScreen = true;
                this.showUpdateScreen = true;
                $(".crud-div").animate({ height: "toggle" });
                $("#tools").removeClass("fix-height-385").addClass("h-555");
            }
            this.clearObjects();
            this.showUpdateScreen = true;
            this.dirtyFlag = false;
        }
    }
    //changes flag on click event to set the height dynamically according to it
    resizeHeightFlag() {
        if (this.testHeightFlag) {
            this.testHeightFlag = false
        }
        else {
            this.testHeightFlag = true;
        }
    }
    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    setLargeListFlag() {
        this.resizeHeightFlag();
        this.changeSizeFromViewPort();
        if (this.showLargeListFlag) {
            this.showLargeListFlag = false;
            this.showEnlargedScreen = false;
            this.showUpdateScreen = true;
        } else {
            this.showLargeListFlag = true;
            this.showEnlargedScreen = true;
            this.showUpdateScreen = false;
        }

    }

    // rcw:comment Shreya |  - Sets the flags when clicks on large/small toggle icon
    togglePage() {
        this.resizeHeightFlag();
        this.changeSizeFromViewPort();
        if (this.showEnlargedScreen) {
            this.showEnlargedScreen = false;
            this.openAddPageFlag = false;
            $("#tools").removeClass("h-555").addClass("fix-height-385");
        } else {
            this.showEnlargedScreen = true;
            $("#tools").removeClass("fix-height-385").addClass("h-555");
        }
        $(".expand-div").removeClass("wide-div");
    }

    // rcw:comment Shreya |  - Changing in the dropdown icon from open to close.
    changeState(id, e) {
        //On click of ul toggle the open/close dropdown
        $("#" + id).next('ul').toggle();
        if ($("#" + id).next('ul').is(':visible')) {
            //Get the first child of the ul and change the caret symbol from open to close and vice versa
            console.log($("#" + id + " i:first-child"))
            $("#" + id + " i:first-child").addClass("open-caret");
            $("#" + id + " i:first-child").removeClass("close-caret");
        } else {
            $("#" + id + " i:first-child").addClass("close-caret");
            $("#" + id + " i:first-child").removeClass("open-caret");
        }
        e.stopPropagation();
        e.preventDefault();
    }

    // rcw:comment Shreya |  - On selecting new rule or changing the rule from rule builder
    ongroupChanged(val) {
        this.dirtyFlag = true;
        this.filter.group = _.cloneDeep(val);
        this.criteriaExp = this.computed(this.filter.group);
        this.eventDetectorObj.criteriaExpression = JSON.stringify(this.filter.group);
    }

    // rcw:comment Shreya |  - Generating the rule expression from the rule json
    computed(group) {
        if (!group) return "";
        for (var str = "(", i = 0; i < group.rules.length; i++) {
            i > 0 && (str += " <strong>" + group.operator + "</strong> ");
            str += group.rules[i].group ?
                this.computed(group.rules[i].group) :
                group.rules[i].field + " " + this.htmlEntities(group.rules[i].condition) + " " + group.rules[i].data;
        }

        return str + ")";
    }

    // rcw:comment Shreya |  - converting into html elements for showing bold and italic numeric value
    htmlEntities(str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    // rcw:comment Shreya |  - Create default timing object on new event detector creation
    generateTimingObj() {
        for (let i = 0; i < 7; i++) {
            let timeObj = new TimeWindow();
            timeObj.timeWindowDay = "" + (i + 1);
            timeObj.startMeridian = "am";
            timeObj.endMeridian = "pm";
            this.timingObjList.push(timeObj);
        }
    }

    // rcw:comment Shreya |  - Remove suppression from the list on add/update event detector.
    removeSuppression(index) {
        this.dirtyFlag = true;
        this.eventDetectorObj.suppressionRule.splice(index, 1);
    }

    // rcw:comment Shreya |  - Convert the server date-time to Client date-time format
    convertDateInAmPm(date) {
        return moment(date, 'HH:mm:ss').format('hh:mm a');
    }

    // rcw:comment Shreya |  - Highlight the suppression on click of the name of suppression displayed on the UI list
    onSuppressionNameClick(tableName, val) {
        if (tableName == 'configTable') {
            if (val == this.selectedConfigSupp) {
                this.selectedConfigSupp = "";
            } else {
                this.selectedConfigSupp = val;
            }
        } else if (tableName == 'simulTable') {
            if (val == this.selectedSimulSupp) {
                this.selectedSimulSupp = "";
            } else {
                this.selectedSimulSupp = val;
            }

        } else if (tableName == 'prodTable') {
            if (val == this.selectedProdSupp) {
                this.selectedProdSupp = "";
            } else {
                this.selectedProdSupp = val;
            }
        }
    }

    // rcw:comment Shreya |  - After drag and drop of suppression search the list based on the drag and drop
    searchData($event: any, tabName: string) {
        if (tabName == "config") {
            var searchCnt = 0;
            for (let item of this.configSuppSearchList) {
                if (item == $event.dragData.suppressionName) {
                    searchCnt++;
                }
            }
            if (searchCnt == 0) {
                this.configSuppSearchList.push($event.dragData.suppressionName);
            }
            this.configurationEventDetectorList = _.cloneDeep(this.finalConfigurationEventDetectorList);
            for (var i = 0; i < this.configurationEventDetectorList.length; i++) {
                if (this.configurationEventDetectorList[i].suppressionRule != null && this.configurationEventDetectorList[i].suppressionRule.length > 0) {
                    var supCount = 0;
                    for (let item of this.configSuppSearchList) {
                        for (let supp of this.configurationEventDetectorList[i].suppressionRule) {
                            if (supp.suppressionName == item) {
                                supCount++;
                            }
                        }
                    }
                    if (supCount != this.configSuppSearchList.length) {
                        this.configurationEventDetectorList.splice(i, 1);
                        i--;
                    }
                } else {
                    this.configurationEventDetectorList.splice(i, 1);
                    i--;
                }
            }
        } else if (tabName == "prod") {
            var searchCnt = 0;
            for (let item of this.prodSuppSearchList) {
                if (item == $event.dragData.suppressionName) {
                    searchCnt++;
                }
            }
            if (searchCnt == 0) {
                this.prodSuppSearchList.push($event.dragData.suppressionName);
            }
            this.productionEventDetectorList = _.cloneDeep(this.finalProductionEventDetectorList);
            for (var i = 0; i < this.productionEventDetectorList.length; i++) {
                if (this.productionEventDetectorList[i].suppressionRule != null && this.productionEventDetectorList[i].suppressionRule.length > 0) {
                    var supCount = 0;
                    for (let item of this.prodSuppSearchList) {
                        for (let supp of this.productionEventDetectorList[i].suppressionRule) {
                            if (supp.suppressionName == item) {
                                supCount++;
                            }
                        }
                    }
                    if (supCount != this.prodSuppSearchList.length) {
                        this.productionEventDetectorList.splice(i, 1);
                        i--;
                    }
                } else {
                    this.productionEventDetectorList.splice(i, 1);
                    i--;
                }
            }
        } else if (tabName == "simul") {
            var searchCnt = 0;
            for (let item of this.simulSuppSearchList) {
                if (item == $event.dragData.suppressionName) {
                    searchCnt++;
                }
            }
            if (searchCnt == 0) {
                this.simulSuppSearchList.push($event.dragData.suppressionName);
            }
            this.simulationEventDetectorList = _.cloneDeep(this.finalSimulationEventDetectorList);
            for (var i = 0; i < this.simulationEventDetectorList.length; i++) {
                if (this.simulationEventDetectorList[i].suppressionRule != null && this.simulationEventDetectorList[i].suppressionRule.length > 0) {
                    var supCount = 0;
                    for (let item of this.simulSuppSearchList) {
                        for (let supp of this.simulationEventDetectorList[i].suppressionRule) {
                            if (supp.suppressionName == item) {
                                supCount++;
                            }
                        }
                    }
                    if (supCount != this.simulSuppSearchList.length) {
                        this.simulationEventDetectorList.splice(i, 1);
                        i--;
                    }
                } else {
                    this.simulationEventDetectorList.splice(i, 1);
                    i--;
                }
            }
        }

    }

    // rcw:comment Shreya |  - After removing the suppression from search again filter according the search fields
    removeSearch(supName, tabName) {
        if (tabName == "config") {
            var index = this.configSuppSearchList.indexOf(supName);
            this.configSuppSearchList.splice(index, 1);
            this.configurationEventDetectorList = _.cloneDeep(this.finalConfigurationEventDetectorList);
            if (this.configSuppSearchList.length > 0) {
                for (var i = 0; i < this.configurationEventDetectorList.length; i++) {
                    if (this.configurationEventDetectorList[i].suppressionRule.length > 0) {
                        var supCount = 0;
                        for (let item of this.configSuppSearchList) {
                            for (let supp of this.configurationEventDetectorList[i].suppressionRule) {
                                if (supp.suppressionName == item) {
                                    supCount++;
                                }
                            }
                        }
                        if (supCount != this.configSuppSearchList.length) {
                            this.configurationEventDetectorList.splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        } else if (tabName == "simul") {
            var index = this.simulSuppSearchList.indexOf(supName);
            this.simulSuppSearchList.splice(index, 1);
            this.simulationEventDetectorList = _.cloneDeep(this.finalSimulationEventDetectorList);
            if (this.simulSuppSearchList.length > 0) {
                for (var i = 0; i < this.simulationEventDetectorList.length; i++) {
                    if (this.simulationEventDetectorList[i].suppressionRule.length > 0) {
                        var supCount = 0;
                        for (let item of this.simulSuppSearchList) {
                            for (let supp of this.simulationEventDetectorList[i].suppressionRule) {
                                if (supp.suppressionName == item) {
                                    supCount++;
                                }
                            }
                        }
                        if (supCount != this.simulSuppSearchList.length) {
                            this.simulationEventDetectorList.splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        } else if (tabName == "prod") {
            var index = this.prodSuppSearchList.indexOf(supName);
            this.prodSuppSearchList.splice(index, 1);
            this.productionEventDetectorList = _.cloneDeep(this.finalProductionEventDetectorList);
            if (this.prodSuppSearchList.length > 0) {
                for (var i = 0; i < this.productionEventDetectorList.length; i++) {
                    if (this.productionEventDetectorList[i].suppressionRule.length > 0) {
                        var supCount = 0;
                        for (let item of this.prodSuppSearchList) {
                            for (let supp of this.productionEventDetectorList[i].suppressionRule) {
                                if (supp.suppressionName == item) {
                                    supCount++;
                                }
                            }
                        }
                        if (supCount != this.prodSuppSearchList.length) {
                            this.productionEventDetectorList.splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        }

    }

    // rcw:comment Shreya |  - Associating Feeds to the selected/new event detector using drag and drop.
    transferFeedDataSuccess($event: any) {
        this.dirtyFlag = true;
        this.eventDetectorObj.feedName = $event.name;
        this.eventDetectorObj.eventType = $event.type;
    }

    // rcw:comment Shreya |  - Associating Consumers to the selected/new event detector using drag and drop.
    transferConsumersDataSuccess($event: any) {
        this.dirtyFlag = true;
        this.eventDetectorObj.consumerName = $event.name;
    }

    // rcw:comment Shreya |  - Associating Suppression to the selected/new event detector using drag and drop.
    transferSuppressionDataSuccess($event: any) {
        this.dirtyFlag = true;
        var count = 0;
        for (let item of this.eventDetectorObj.suppressionRule) {
            if ($event.id == item.suppressionId) {
                count++;
            }
        }
        if (count == 0) {
            let suppressionObj = new Suppression();
            suppressionObj.suppressionName = $event.name;
            suppressionObj.suppressionId = $event.id;
            suppressionObj.priority = this.eventDetectorObj.suppressionRule.length + 1;
            this.eventDetectorObj.suppressionRule.push(suppressionObj);
        }
    }

    // rcw:comment Shreya |  - Change the index of the selected day to display the associated AM/PM value
    changeModel(index) {
        this.timeIndex = index;
        this.timingObjList[index].timeWindowDay = index + 1;
    }

    // rcw:comment Shreya |  - Add date to list and convert the format in which we need to send to the database
    addDateToArray(val) {
        let date = moment(val).format('YYYY-MM-DD');
        let datCnt = 0;
        for (let item of this.eventDetectorObj.exclusionDates) {
            if (item.exclusionDate == date) {
                datCnt++;
                break;
            }
        }
        if (datCnt == 0) {
            let exculeDateObj = new ExclusionDate();
            if (this.eventDetectorObj.eventDetectionId) {
                exculeDateObj.eventDetectionId = this.eventDetectorObj.eventDetectionId;
            } else {
                exculeDateObj.eventDetectionId = null;
            }
            exculeDateObj.exclusionDate = date;
            exculeDateObj.recursive = false;
            this.eventDetectorObj.exclusionDates.push(exculeDateObj);
        }
        this.excludeDateVal = "";
        this.dirtyFlag = true;
    }

    // rcw:comment Shreya |  - Add/update EventDetector if it satisfies all the validation coming from UI
    onSubmit(eventDetectorForm) {
        this.submittedFlag = true;
        for (let item of this.timingObjList) {
            this.timingValidFlag = this.checkDate(item.startHrTime, item.startMinTime, item.startMeridian, item.endHrTime, item.endMinTime, item.endMeridian);
            if (!this.timingValidFlag) {
                break;
            }
        }
        if (eventDetectorForm.form.valid && this.eventDetectorObj.consumerName != null && this.eventDetectorObj.consumerName != "" && this.timingValidFlag == true && this.eventDetectorObj.criteriaExpression != null && this.eventDetectorObj.criteriaExpression != "") {
            this.isRequesting = true;
            this.submittedFlag = false;
            let timingList: TimeWindow[] = [];
            for (let item of this.timingObjList) {
                let timing = new TimeWindow();
                if (item.startHrTime && item.startMinTime && item.endHrTime && item.endMinTime) {
                    timing.startTime = moment(item.startHrTime + ":" + item.startMinTime + ":00 " + item.startMeridian, ["h:mm:ss A"]).format("HH:mm:ss");
                    timing.endTime = moment(item.endHrTime + ":" + item.endMinTime + ":00 " + item.endMeridian, ["h:mm:ss A"]).format("HH:mm:ss");
                    timing.timeWindowDay = item.timeWindowDay;
                    timing.startMeridian = undefined;
                    timing.endMeridian = undefined;
                    timingList.push(timing);
                }
            }
            this.eventDetectorObj.timeWindow = timingList;
            if (this.eventDetectorObj.suppressionRule != null && this.eventDetectorObj.suppressionRule.length > 0) {
                this.eventDetectorObj.suppressionsAsnBO = [];
                let i = 0;
                for (let item of this.eventDetectorObj.suppressionRule) {
                    let suppAsn = new SuppressionAsnBo();
                    suppAsn.orderOfExecution = ++i;
                    suppAsn.skipIndicator = 1;
                    suppAsn.suppressionId = item.suppressionId;
                    suppAsn.eventDetectorId = this.eventDetectorObj.eventDetectionId;
                    this.eventDetectorObj.suppressionsAsnBO.push(suppAsn);
                }
            }
            this.addOrUpdateEventDetector();
        }
    }

    //rcw:comment Shreya | -Add/Update Event detectors calling backend API
    addOrUpdateEventDetector() {
        if (this.eventDetectorObj.eventDetectionId) {
            delete this.eventDetectorObj['_$visited'];
            var ctrl = this;
            this.eventDetectorService.update(this.eventDetectorObj).then(function (updatedEds) {
                ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector updated successfuly');
                if (ctrl.selectedIndex == -1) {
                    ctrl.getAllEventDetectors();
                } else {
                    if (ctrl.selectedTab == "config") {
                        for (let i = 0; i < ctrl.configurationEventDetectorList.length; i++) {
                            if (ctrl.configurationEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.configurationEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                        for (let i = 0; i < ctrl.finalConfigurationEventDetectorList.length; i++) {
                            if (ctrl.finalConfigurationEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.finalConfigurationEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab == "simul") {
                        for (let i = 0; i < ctrl.simulationEventDetectorList.length; i++) {
                            if (ctrl.simulationEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.simulationEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                        for (let i = 0; i < ctrl.finalSimulationEventDetectorList.length; i++) {
                            if (ctrl.finalSimulationEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.finalSimulationEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                    } else if (ctrl.selectedTab == "produc") {
                        for (let i = 0; i < ctrl.productionEventDetectorList.length; i++) {
                            if (ctrl.productionEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.productionEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                        for (let i = 0; i < ctrl.finalProductionEventDetectorList.length; i++) {
                            if (ctrl.finalProductionEventDetectorList[i].eventDetectionId == updatedEds.eventDetectionId) {
                                ctrl.finalProductionEventDetectorList[i] = updatedEds;
                                break;
                            }
                        }
                    }
                }
            }).then(function () {
                Object.keys(ctrl.form.form.controls).forEach(control => {
                    ctrl.form.form.controls[control].markAsPristine();
                });
                ctrl.isRequesting = false;
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
                } else {
                    ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector updation failed');
                }
                ctrl.isRequesting = false;
            });

        } else {
            var ctrl = this;
            this.eventDetectorService.add(this.eventDetectorObj).then(function (addedEds) {
                ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector creation successfully');
                if (ctrl.selectedTab == "config") {
                    ctrl.configurationEventDetectorList.push(addedEds);
                    ctrl.finalConfigurationEventDetectorList.push(addedEds);
                } else if (ctrl.selectedTab == "simul") {
                    ctrl.simulationEventDetectorList.push(addedEds);
                    ctrl.finalSimulationEventDetectorList.push(addedEds);
                } else if (ctrl.selectedTab == "produc") {
                    ctrl.productionEventDetectorList.push(addedEds);
                    ctrl.finalProductionEventDetectorList.push(addedEds);
                }
            }).then(function () {
                ctrl.clearObjects();
                ctrl.isRequesting = false;
            }).catch(function (error) {
                if (error && error._body) {
                    ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
                } else {
                    ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector creation failed');
                }
                ctrl.isRequesting = false;
            });
        }
    }

    // rcw:comment Shreya |  - This method is for save and publish the EventDetector after all the validations coming from UI
    onPublish(eventDetectorForm) {
        this.submittedFlag = true;
        for (let item of this.timingObjList) {
            this.timingValidFlag = this.checkDate(item.startHrTime, item.startMinTime, item.startMeridian, item.endHrTime, item.endMinTime, item.endMeridian);
            if (!this.timingValidFlag) {
                break;
            }
        }
        if (eventDetectorForm.form.valid && this.eventDetectorObj.consumerName != null && this.eventDetectorObj.consumerName != "" && this.timingValidFlag == true && this.eventDetectorObj.criteriaExpression != null && this.eventDetectorObj.criteriaExpression != "") {
            this.isRequesting = true;
            this.submittedFlag = false;
            let timingList: TimeWindow[] = [];
            for (let item of this.timingObjList) {
                let timing = new TimeWindow();
                if (item.startHrTime && item.startMinTime && item.endHrTime && item.endMinTime) {
                    timing.startTime = moment(item.startHrTime + ":" + item.startMinTime + ":00 " + item.startMeridian, ["h:mm:ss A"]).format("HH:mm:ss");
                    timing.endTime = moment(item.endHrTime + ":" + item.endMinTime + ":00 " + item.endMeridian, ["h:mm:ss A"]).format("HH:mm:ss");
                    timing.timeWindowDay = item.timeWindowDay;
                    timing.startMeridian = undefined;
                    timing.endMeridian = undefined;
                    timingList.push(timing);
                }
            }
            this.eventDetectorObj.timeWindow = timingList;
            if (this.eventDetectorObj.suppressionRule != null && this.eventDetectorObj.suppressionRule.length > 0) {
                this.eventDetectorObj.suppressionsAsnBO = [];
                let i = 0;
                for (let item of this.eventDetectorObj.suppressionRule) {
                    let suppAsn = new SuppressionAsnBo();
                    suppAsn.orderOfExecution = ++i;
                    suppAsn.skipIndicator = 1;
                    suppAsn.suppressionId = item.suppressionId;
                    suppAsn.eventDetectorId = this.eventDetectorObj.eventDetectionId;
                    this.eventDetectorObj.suppressionsAsnBO.push(suppAsn);
                }
            }
            this.publishEventDetectors();
        }
    }

    //rcw:comment Shreya | -Publish the event detector calling backend API
    publishEventDetectors() {
        delete this.eventDetectorObj['_$visited'];
        var ctrl = this;
        this.eventDetectorService.publish(this.eventDetectorObj).then(function (publishedEds) {
            ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector published successfully');
            if (ctrl.eventDetectorObj.eventDetectionId == null) {
                if (ctrl.selectedTab == "config") {
                    for (let i = 0; i < ctrl.configurationEventDetectorList.length; i++) {
                        if (ctrl.configurationEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.configurationEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                    for (let i = 0; i < ctrl.finalConfigurationEventDetectorList.length; i++) {
                        if (ctrl.finalConfigurationEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.finalConfigurationEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                } else if (ctrl.selectedTab == "simul") {
                    for (let i = 0; i < ctrl.simulationEventDetectorList.length; i++) {
                        if (ctrl.simulationEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.simulationEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                    for (let i = 0; i < ctrl.finalSimulationEventDetectorList.length; i++) {
                        if (ctrl.finalSimulationEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.finalSimulationEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                } else if (ctrl.selectedTab == "produc") {
                    for (let i = 0; i < ctrl.productionEventDetectorList.length; i++) {
                        if (ctrl.productionEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.productionEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                    for (let i = 0; i < ctrl.finalProductionEventDetectorList.length; i++) {
                        if (ctrl.finalProductionEventDetectorList[i].eventDetectionId == publishedEds.eventDetectionId) {
                            ctrl.finalProductionEventDetectorList[i] = publishedEds;
                            break;
                        }
                    }
                }
            } else {
                if (ctrl.selectedIndex == -1) {
                    ctrl.getAllEventDetectors();
                } else {
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationEventDetectorList[ctrl.selectedIndex] = publishedEds;
                        ctrl.finalConfigurationEventDetectorList[ctrl.selectedIndex] = publishedEds;
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationEventDetectorList[ctrl.selectedIndex] = publishedEds;
                        ctrl.finalSimulationEventDetectorList[ctrl.selectedIndex] = publishedEds;
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionEventDetectorList[ctrl.selectedIndex] = publishedEds;
                        ctrl.finalProductionEventDetectorList[ctrl.selectedIndex] = publishedEds;
                    }
                }
            }

        }).then(function () {
            Object.keys(ctrl.form.form.controls).forEach(control => {
                ctrl.form.form.controls[control].markAsPristine();
            });
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector publishion failed');
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Whenever user clicks the below arrow the table scroll downs
    scrollDownTable() {
        $('#tabContentMainDiv').animate({
            scrollTop: '+=100'
        }, 100);
    }

    // rcw:comment Shreya |  - Whenever user clicks the below arrow the table scroll up
    scrollUpTable() {
        $('#tabContentMainDiv').animate({
            scrollTop: '-=100'
        }, 100);
    }

    // rcw:comment Shreya |  - Multiple Date validation for AM/PM
    checkDate(startHrTime, startMinTime, startMeridian, endHrTime, endMinTime, endMeridian) {
        //If start time and end time is present then only go ahead else error
        if (startHrTime && startMinTime && endHrTime && endMinTime) {
            let startDate = new Date();
            let endDate = new Date();
            //If startMeridian is AM then the time is in 12 hr format itself so no need of conversion
            if (startMeridian == "AM") {
                startDate.setHours(startHrTime);
                startDate.setMinutes(startMinTime);
            } else {
                //If it is PM then we need to convert the time in 24 hour format by adding to 12 to the selected hr time
                startDate.setHours(12 + parseInt(startHrTime));
                startDate.setMinutes(parseInt(startMinTime));
            }
            //If endMeridian is AM then the time is in 12 hr format itself so no need of conversion
            if (endMeridian == "AM") {
                endDate.setHours(endHrTime);
                endDate.setMinutes(endMinTime);
            } else {
                //If it is PM then we need to convert the time in 24 hour format by adding to 12 to the selected hr time
                endDate.setHours(12 + parseInt(endHrTime));
                endDate.setMinutes(parseInt(endMinTime));
            }
            //If statdate is less than endDate then it should show message as it is invalid.Hence return false
            if (startDate > endDate) {
                return false;
            } else {
                return true;
            }
        } else {
            //If any of them is not present than also the date is invalid
            if (startHrTime || startMinTime || endHrTime || endMinTime) {
                return false;
            } else {
                return true;
            }
        }
    }

    // rcw:comment Shreya |  - Remove the date from the selected date list for add/update
    removeExcludeDates(index) {
        this.eventDetectorObj.exclusionDates.splice(index, 1);
        this.dirtyFlag = true;
    }

    // rcw:comment Shreya |  - If there are any changes open confirmation box
    checkCancelChanges() {
        if (this.selectedIndex !== -1) {
            this.openCancelConfirmation(this.eventDetectorObj, 'cancelEdChanges', -1);
        } else {
            //If it is newly created usecase it clears the form
            this.clearObjects();
        }
    }

    // rcw:comment Shreya |  - Cancels the changes on the EventDetector form page if form is dirty
    // the revert to original state else clear the form
    cancelEventDetector() {
        //If the EventDetector is for updation cancels the new added changes
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        if (this.selectedIndex !== -1) {
            if (this.selectedTab == "config") {
                this.eventDetectorObj = _.cloneDeep(this.configurationEventDetectorList[this.selectedIndex]);
            } else if (this.selectedTab == "simul") {
                this.eventDetectorObj = _.cloneDeep(this.simulationEventDetectorList[this.selectedIndex]);
            } else if (this.selectedTab == "produc") {
                this.eventDetectorObj = _.cloneDeep(this.productionEventDetectorList[this.selectedIndex]);
            }
            this.filter.group = JSON.parse(this.eventDetectorObj.criteriaExpression);
            this.criteriaExp = this.computed(this.filter.group);
            this.timingObjList = [];
            this.generateTimingObj();
            if (this.eventDetectorObj.timeWindow != undefined) {
                for (let item of this.eventDetectorObj.timeWindow) {
                    var start = moment(item.startTime, 'HH:mm:ss').format('hh:mm:ss a');
                    var end = moment(item.endTime, 'HH:mm:ss').format('hh:mm:ss a');
                    var startMeri = moment(item.startTime, 'HH:mm:ss').format('a');
                    var endMeri = moment(item.endTime, 'HH:mm:ss').format('a');
                    var starttime = start.split(':');
                    var endtime = end.split(':');
                    let timing = new TimeWindow();
                    timing.startHrTime = starttime[0];
                    timing.startMinTime = starttime[1];
                    timing.startMeridian = startMeri;
                    timing.endHrTime = endtime[0];
                    timing.endMinTime = endtime[1];
                    timing.endMeridian = endMeri;
                    timing.timeWindowDay = item.timeWindowDay;
                    // rcw:update | item.timeWindowDay is string and should cast to number
                    this.timingObjList[Number(item.timeWindowDay) - 1] = timing;
                }
            }
            this.cancelEdDisplay = false;
        } else {
            //If it is newly created usecase it clears the form
            this.clearObjects();
        }
        console.log("this.eventDetectorObj :", this.eventDetectorObj)
    }

    // rcw:comment Shreya | -Generate next version of selected event detector and call backend API
    generateNextVersion(eventDetector: EventDetector) {
        var ctrl = this;
        this.isRequesting = true;
        this.eventDetectorService.nextVersion(eventDetector.eventDetectionId).then(function (newVersionEd) {
            ctrl.toasterService.sendToaster('success', 'Event Detector', 'New Version created successfully');
            if (ctrl.selectedTab == "config") {
                ctrl.configurationEventDetectorList.push(newVersionEd);
                ctrl.finalConfigurationEventDetectorList.push(newVersionEd);
            } else if (ctrl.selectedTab == "simul") {
                ctrl.simulationEventDetectorList.push(newVersionEd);
                ctrl.finalSimulationEventDetectorList.push(newVersionEd);
            } else if (ctrl.selectedTab == "produc") {
                ctrl.productionEventDetectorList.push(newVersionEd);
                ctrl.finalProductionEventDetectorList.push(newVersionEd);
            }
        }).then(function () {
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Event Detector', 'New version creation failed');
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya | -Duplicate selected event detector and call backend API
    duplicateEventDetector(eventDetector: EventDetector) {
        var ctrl = this;
        this.isRequesting = true;
        this.eventDetectorService.copy(eventDetector.eventDetectionId).then(function (duplicatedEd) {
            ctrl.toasterService.sendToaster('success', 'Event Detector', 'Event Detector duplicated successfully');
            if (ctrl.selectedTab == "config") {
                ctrl.configurationEventDetectorList.push(duplicatedEd);
                ctrl.finalConfigurationEventDetectorList.push(duplicatedEd);
            } else if (ctrl.selectedTab == "simul") {
                ctrl.simulationEventDetectorList.push(duplicatedEd);
                ctrl.finalSimulationEventDetectorList.push(duplicatedEd);
            } else if (ctrl.selectedTab == "produc") {
                ctrl.productionEventDetectorList.push(duplicatedEd);
                ctrl.finalProductionEventDetectorList.push(duplicatedEd);
            }
        }).then(function () {
            ctrl.isRequesting = false;
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.toasterService.sendToaster('error', 'Event Detector', JSON.parse(error._body).error_description);
            } else {
                ctrl.toasterService.sendToaster('error', 'Event Detector', 'Event Detector duplication failed');
            }
            ctrl.isRequesting = false;
        });
    }

    // rcw:comment Shreya |  - Generates the dynamic context menu for the right click on the EventDetector name for the various actions like new version,Delete,Archive,Duplicate
    public onContextMenu($event: MouseEvent, item: any): void {
        this.configMenuOptions = [];
        this.configMenuOptions.push({
            html: () => 'Version',
            click: (item) => {
                this.generateNextVersion(item);
            },
            enabled: (item): boolean => {
                return item.versionable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Duplicate',
            click: (item) => {
                this.duplicateEventDetector(item);
            },
            enabled: (item): boolean => {
                return item.copyable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Archive',
            click: (item) => {
                this.openArchiveConfirmation(item);
            },
            enabled: (item): boolean => {
                return item.archivable;
            }
        })
        this.configMenuOptions.push({
            html: () => 'Delete',
            click: (item) => {
                this.openDeleteConfirmation(item);
            }, enabled: (item): boolean => {
                return item.deletable;
            }
        })
        this.contextMenuService.show.next({
            actions: this.configMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }

}
