import {Suppression} from "../suppression/suppression";
import {SuppressionAsnBo} from "../suppression/suppression";
import {TimeWindow} from "../models/timeWindow";

export class EventDetector {
    public id: number;
    public eventDetectionId: number;
    public description: string;
    public eventName: string;
    public feedName: string;
    public feedType: string;
    public eventType: string;
    public consumerName: string;
    public maxLatencyUnit: string;
    public maxLatency: number;
    public version: number;
    public status: string;
    public priority: number;
    public isEditable: boolean;
    public isInProduction: boolean;
    public isInSimulation: boolean;
    public updatable: boolean;
    public deletable: boolean;
    public archivable: boolean;
    public copyable: boolean;
    public versionable: boolean;
    public environment: string;
    public suppressionRule:Suppression[];
    public suppressionsAsnBO:SuppressionAsnBo[];
    public timeWindow:TimeWindow[];
    public exclusionDates:ExclusionDate[];
    public criteriaExpression:string;
    public publishable:boolean;
    public isSelected:boolean;
    
    constructor() {
        this.eventDetectionId = null;
        this.status = "DRAFT";
        this.maxLatencyUnit = "DAYS";
        this.suppressionsAsnBO = [];
        this.suppressionRule = [];
        this.updatable = true;
        this.deletable = false;
        this.archivable = false;
        this.copyable = false;
        this.versionable = false;
        this.publishable = true;
        this.exclusionDates = [];
        this.isSelected = false;
    };
}

export class ExclusionDate{
    
    public eventDetectionId:number;
    public exclusionDate:string;
    public recursive:boolean;
    
    constructor() {};
}