import {Component, OnInit,AfterViewInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Common} from '../common/common';

@Component({
    selector: 'coming-soon',
    templateUrl: './comingsoon.component.html',
    styleUrls: ['./comingsoon.component.css']
})

export class ComingSoonComponent implements OnInit,AfterViewInit {

    currentFeature: String = "Default";
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private common: Common) {
        this.route.params.subscribe(params => {
            this.currentFeature = params['featureName'];
        });
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        this.changeSizeFromViewPort();
    }

    changeSizeFromViewPort() {
        var panelGot = this.common.winResize('menuJ', null);
        $('.comingsoon-wrapper').css('height', panelGot - 20);
    }

}



