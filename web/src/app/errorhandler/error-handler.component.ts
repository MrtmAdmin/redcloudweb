import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { ErrorHandler } from './error-handler';
import { Constant } from '../common/constant';
import { AuthService } from '../login/authentication.service';
import { Common } from '../common/common';

@Component({
    moduleId: module.id,
    templateUrl: 'error-handler.component.html'

})

export class ErrorHandlerComponent implements OnInit {

    constructor(private route: ActivatedRoute,
        private router: Router,
        private errorHandler: ErrorHandler,
        private activatedRoute: ActivatedRoute,
        private constant: Constant,
        private common: Common,
        private authService: AuthService) {
    }

    public errorContent: string;
    public errorHeader: string;
    public errorButtonName: string
    public errorButtonLink: string;

    ngOnInit() {

        this.activatedRoute.queryParams.subscribe(params => {
            let actionValue = params['action'];
            if (actionValue == "sessionTimeout") {
                this.errorContent = "You need to login again";
                this.errorHeader = "Session Expired";
                this.errorButtonName = "Go to Login";
                this.errorButtonLink = this.constant.loginUrl;
            }
            else {
                this.errorContent = this.errorHandler.content;
                this.errorHeader = this.errorHandler.header;
                this.errorButtonName = this.errorHandler.buttonName;
                this.errorButtonLink = this.errorHandler.buttonLink;
            }
        });
    }


    onClickButton() {
        this.router.navigate([this.errorButtonLink]);
    }


}