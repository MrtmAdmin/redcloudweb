import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BaseRequestOptions,XHRBackend} from '@angular/http';
import {AppComponent} from './app.component';
import {FooterComponent} from '../app/footers/footer.component';
import {ConfirmDialogModule, ConfirmationService} from 'primeng/primeng';
import {HttpService} from '../app/services/http.service';
import {Constant} from '../app/common/constant';
import {Common} from '../app/common/common';
import {DialogModule} from 'primeng/primeng';

// rcw:added | Due to angular 2 Error: httpFactory lambada exported before uses
export function httpFactory(backend, options) {
  return  new HttpService(backend, options);
}

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                ConfirmDialogModule,
                DialogModule
            ],
            declarations: [
                AppComponent,
                FooterComponent
            ],
            providers: [
                ConfirmationService,
                Constant,
                Common,
                {
                    provide: HttpService,
                    useFactory: httpFactory,
                    deps: [XHRBackend, BaseRequestOptions]
                }
            ]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        console.log("Create App Component");
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

});
