/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Injectable} from "@angular/core";

@Injectable()
export class Constant {

    public NO_PAGE_ACCESS: string;
    public READ_ONLY: string;
    public UPDATE_ACCESS: string;
    public loginUrl: string;
    public errorUrl: string;
    public forgotPasswordUrl: string;
    public resetPasswordUrl: string;
    public dashboardUrl: string;
    public defaultAccessRights: string[];
    public idleTimeout:number;
    public tokenName:string;
    public refreshToken:string;
    public userObjName:string;
    public userProfilePic:string;

    constructor() {
        this.NO_PAGE_ACCESS = 'NO_PAGE_ACCESS';
        this.READ_ONLY = 'READ_ONLY';
        this.UPDATE_ACCESS = 'UPDATE_ACCESS';
        this.loginUrl = '/login';
        this.errorUrl = '/error';
        this.dashboardUrl = '/dashboard';
        this.forgotPasswordUrl = '/forgotpassword';
        this.resetPasswordUrl = '/resetpassword';
        this.defaultAccessRights = ['/', '/forgotpassword', '/resetpassword','/changepassword' , '/dashboard', '/users', '/error'];
        this.idleTimeout = 1800;
        this.tokenName = 'access_token';
        this.refreshToken = 'refresh_token';
        this.userObjName = 'user';
        this.userProfilePic='userprofilepicbase64'
    };
}