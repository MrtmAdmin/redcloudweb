//Onclick Show side menu
function openHamburger() {
    $(".sidemenuHumburgerOpen").fadeIn(1000);
    $(".sidemenuOpenLeft").show(100).css("left", "-15px");
    $('body').css('overflow', 'hidden');
}
//Onclick Hide side menu
function closeHamburger() {
    $(".sidemenuHumburgerOpen").fadeOut(1000);
    $(".sidemenuOpenLeft").css("left", "");
}

function expandDiv(elemId, collapsDivId) {
    $("." + elemId).toggleClass('wide-div');
    console.log($("." + collapsDivId));
    console.log("expand collapsDivId :" + collapsDivId)
    $("." + collapsDivId).animate({height: "toggle"});
}

$(document).ready(function () {
    $(document).mouseup(function (e)
    {
        hideDiv(e);
    });
});

function hideDiv(e) {
    var container = $(".speech");

    if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
}
